package nowhere2gopp.main;

import nowhere2gopp.board.*;
import nowhere2gopp.io.*;
import nowhere2gopp.preset.*;
import nowhere2gopp.player.*;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.*;

/**Ausführbare Klasse zum Spielen von Nowhere2Go.
*Alle Spiele Parameter werden über den Programmaufruf mitgegeben und von einem
*{@link nowhere2gopp.preset.ArgumentParser} verarbeitet. Falls unvollständige Parameter übergeben wurde
*wird eine Fehlermeldung erzeugt und das Programm beendet. Treten während des Spielens
*Fehler auf führen auch diese zur Beendigung des Programms. 
*
*<h2>Standardwerte für Parameter</h2>
*
*<p>
*Für den Serverbetrieb wird nur eine grafische Ausgabe verwendet<br>
*Für den Klientbetrieb erhält der Spieler seine eigene Ausgabe<br>
*Zwischen jedem Zug sind 200ms Wartezeit gesetzt<br>
*Jeder {@link nowhere2gopp.player.HumanPlayer} hat eine grafische Eingabe<br>
*</p>
*/
public class NoWhereToGo{
	/**Player Referenz für den monentan ziehenden Spieler*/
	private Player currPlayer = null;
	/**Player Referenz für den gegnerischen Spieler*/
	private Player oppPlayer = null;
	/**Player Referenz zum tuaschen von aktuellem und gegnerischen Spieler*/
	private Player swapPlayer;

	/**Board auf dem die main spielt*/
	private Board board;
	/**Graphische Ausgabe und/oder Eingabe des Spielbrettes*/
	private GUI gui = null;
	/**Textbasierte Eingabemöglichkeit*/
	private TextInput textInput = null;
	/**Parser zum verarbeiten der Konsoleneingaben. 
	*Siehe {@link nowhere2gopp.preset.ArgumentParser} */
	private ArgumentParser parser;
	/**Die Verzögerung ist standardmässig auf 200ms gesetzt.*/
	private int delay = 200;
	/**Flag um zwei Netzwerk Spieler verarbeiten zu können.
	*Ist true wenn beide Spieler vom Typ {@link nowhere2gopp.preset.PlayerType#Remote} sind.*/
	private boolean remoteOneisSet = false;
	/**Im Fall wenn zwei Kienten angibt ob der erste Gesetzt wurde.*/
	private boolean host1Set = false;
	/**Lokale {@link Registry} an die Spieler angeboten oder in der ein Spieler gefunden werden kann.*/
	private Registry localR;
	/**Lokale {@link Registry} die an die Spieler angeboten oder in der ein Spieler gefunden werden kann.
	*Wird verwendet wenn ein Server zwei Netzwerkspieler hat.*/
	private Registry localR2;
	/**Falls zur Darstellung einer Grafischen Ausgabe der einzelnen Spieler.
	*Nicht mit der Grafischen Ausgabe des Hauptspielfelds {@link NoWhereToGo#board} zu verwechseln,
	*welches immer angezeigt wird.*/
	private boolean showGrafic;

	/**
	*Erzeugt neues NoWhereToGo Spiel.
	*Dabei werden die Kommandozeilen Argumente von einem {@link ArgumentParser}
	*verabeitet. Zudem wird einen {@code LocateRegistry} erzeugt, falls es sich um einen
	*Klienten handelt oder die {@code LocateRegistry} eines Host übernommen wird.
	*@param args
    *		Argumente für den Parser
    *@throws ArgumentParserException
    *		wenn das parsen der Argument fehlschlägt
    *@throws RemoteException
    *		wenn es einen Fehler bei der {@code LocateRegistry} gibt*/
	public NoWhereToGo(String[] args) throws ArgumentParserException, RemoteException{

		/*Hier werden alle Einstellungen und flags gespeichert.*/
		parser = new ArgumentParser(args);

		/*Feststellen ob es sich um einen lokales Spiel oder einen Kienten handelt.*/
		if(!parser.isSet("offer")){
			/*Der Server hat standardmässig keine zusätzliche grafische Anzeige für die Spieler.*/
			showGrafic = false;
			/*Falls doch eine grafische Ausgabe für jeden nicht Remote Spieler gewünscht ist.
			*Diese wird dann von dem localen Board aus erzeugt.
			*/
			if(parser.grafic())
				showGrafic = true;
		}else {
			/*Ein Klient hat standarsmässig einen grafische Ausgabe.*/
			showGrafic = true;
			if(parser.noGrafic())
				showGrafic = false;
			/*Wenn ein Spieler zur Verfügung gestellt wird, muss eine LocateRegistry erstellt werden.*/
			localR = LocateRegistry.createRegistry(parser.getPort());
		}
		/*Wenn ein Host Name übergeben wird die entsprechende LocateRegistry abgerufen.*/
		if(parser.isSet("host")){
			localR = LocateRegistry.getRegistry(parser.getHost(), parser.getPort());
		}
		/*Wurde ein zweiter Host Name übergeben wird die entsprechende LocateRegistry abgerufen.*/
		if(parser.isSet("host2")){
			localR2 = LocateRegistry.getRegistry(parser.getHost2(), parser.getPort2());
		}
		/*Ist ein Delay gesetzt wird der Standartwert von 200ms überschrieben.*/
		if(parser.isSet("delay"))
			delay = parser.getDelay();
	}
	/**Hauptfunktion, die Spieler anhand der übergebenen Parameter erzeugt.
	*Tritt während der Spielphasen oder bei der Initialisierung der Spieler ein Fehler auf,
	*wird einen Fehlermeldung erzeugt und die Funktion beendet.
	*@param args
	*		Spielparameter
	*/
	public static void main(String[] args) {
		/*Erzeugung eines neuen Spiels*/
		NoWhereToGo game;
		try{
			game = new NoWhereToGo(args);
		}catch(ArgumentParserException a){
			System.err.println("Fehler beim lesen des Programmaufrufs: " + a);
			return;
		}catch (RemoteException e) {
			System.err.println("Fehler beim Aufbau des Netzwerks");
			return;
		}

		/*Initialisieren des Spieler entspechend der Parameter. Handelt es sich um den Spielserver
		* wird dieser Spielzyklus durchlaufen.*/
		try{
			if(game.init()){
				game.play();
				game.gui.printBoard();
			}
		}catch (AccessException e) {
			System.err.println(e.getMessage());
			return ;
		}catch (java.rmi.ConnectIOException e) {
			System.err.println("Es konnte keine Verbindung zum Klienten hergestellt werden." + 
				" Überprüfen Sie ob Sie die richtige IP Adresse des Klienten übergeben haben!");
			return ;
		}catch (java.rmi.ConnectException e) {
			System.err.println("Es konnte keine Verbindung zum Klienten hergestellt werden." + 
				" Überprüfen Sie ob Sie den richtigen Port des Klienten übergeben haben"+
				" oder der Klient sein Spiel beendet hat!");
			return ;
		}catch (RemoteException e) {
			System.err.println("Es ist ein Fehler bei der Kommunikation mit dem "+
				"Klienten aufgetreten!");
			return ;
		}catch (AlreadyBoundException e){
			System.err.println(e.getMessage());
			return ;
		}catch (NotBoundException e) {
			System.err.println(e.getMessage());
			return ;
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			return;
		}
	}

	/**Bietet der {@code LocateRegistry} einen Spieler mit an.
	*@param player 
	*		Spielerreferenz die angeboten wird
	*@param name
	*		Name unter dem der Spieler angeboten wird
	*@throws Exception
	*		wird geworfen wenn unter dem Namen bereits ein Spieler gebunden ist 
	*		oder wenn der Zugriff verweigert wurde.*/
	private void offer(Player player, String name) throws Exception{
		try {
			if(parser.isSet("host2")){
				localR2.bind( name , player);
			}else{
				localR.bind( name , player);
			}
			System.out.println("Player (" + name + ") ready");
		}catch (AlreadyBoundException e){
			throw new AlreadyBoundException("Player mit Namen (" + name + 
				") ist bereits registriert");
		}catch (AccessException e) {
			throw new AccessException("Zugriff auf den Spieler (" + name + ") verweigert!");
		}
	}
	/**Gibt eine Referenz auf einen Spieler zurück der unter dem Namen angemeldet ist.
	*@return Die gefundene Spielerrefferenz
	*@param name
	*		Name des gesuchen Spielers
	*@throws Exception
	*		wirft alle Exceptions von {@link java.rmi.registry.Registry#lookup(String name)}*/
	private Player find( String name) throws Exception{
		Player player = null;
		try{
			if(remoteOneisSet && parser.isSet("host2")){
				player = (Player)localR2.lookup(name);
			}else {
				player = (Player)localR.lookup(name);
			}
			System.out.println("Player (" + name + ") found");
		}catch (NotBoundException e) {
			throw new NotBoundException("Player mit Namen (" + name + 
				") wurde noch nicht registriert");
		}
		return player;
	}

	/**Gibt einen Spieler des übergenen Typs zurück. Dabei wird auf die in den Parametern des
	*Programmaufrufs spezifizierte Ausgabeart geachtet. 
	*@return Spieler des gewünschten Typs
	*@param type
	*		Type des Spielers
	*@throws RemoteException
	*		wird geworfen wenn beim Zugriff bei einem Netzwerkspieler ein Fehler auftritt
	*@throws Exception
	*		wirft die Exceptions der verwendeten Methoden weiter*/
	private Player creatPlayer(PlayerType type)
		throws RemoteException, Exception{
		switch (type) {
			case Human:
				Requestable input = gui;
				if(parser.textinput())
					input = new TextInput();
				if(showGrafic)
					return new HumanPlayer(input, gui);
				return new HumanPlayer(input);
			case RandomAI:
				if(showGrafic)
					return new RandomAIPlayer(gui);
				return new RandomAIPlayer();
			case SimpleAI:
				if(showGrafic)
					return new SimpleAIPlayer(gui);
				return new SimpleAIPlayer();
			case AdvancedAI:
				if(showGrafic)
					return new AdvancedAIPlayer(gui);
				return new AdvancedAIPlayer();
			case EnhancedAI:
				if(showGrafic)
					return new EnhancedAIPlayer(gui);
				return new EnhancedAIPlayer();
			case Remote:
				String name = "";
				/*Nur ein Klient*/
				if( parser.isSet("host") ){
					name = "Klient";
				}
				/*Zweiter Klient*/
				if(remoteOneisSet && parser.isSet("host2")){
					name = "Klient2";
					return find(name);
				}
				return find(name);
			default:
				throw new Exception("Inkorrekter SpielerTyp übergeben");
		}
	}

	/**
	*Initialisiert alle Spieler so wie das Spielbrett. Liefert wahr zurück wenn es sich um der
	*Server handelt und falsch wenn einen Kilientenspieler initialisiert wurde.
	*@return wahr für Server, falsch für Klient.
	*@throws ArgumentParserException
	*		wenn beim Abfragen der Parameter ein Fehler auftritt.
	*@throws RemoteException
	*		wenn bei der Kommunikation mit dem Netzwerk ein Fehler auftritt.
	*@throws Exception 
	*		wirft die Exceptions der übrigen Funtionen weiter an die main Methode
	*/
	private boolean init()
		throws ArgumentParserException, RemoteException, Exception{
		/*Initialisiere einen Klienten. Hier wird nur ein Spieler verwendet*/
		if (parser.isSet("offer")) {
			currPlayer = creatPlayer(parser.getOffer());
			Player net = new RemotePlayer(currPlayer);
			if(parser.secondClient()){
				offer(net, "Klient2");
				return false;
			}
			offer(net, "Klient");
			return false;
		}
		board = new Board(parser.getSize());
		setGui(board.viewer());

		currPlayer = creatPlayer(parser.getRed());
		if(parser.getBlue() == PlayerType.Remote && parser.getRed() == PlayerType.Remote)
			remoteOneisSet = true;
		oppPlayer = creatPlayer(parser.getBlue());

		currPlayer.init(parser.getSize(), PlayerColor.Red);
		oppPlayer.init(parser.getSize(), PlayerColor.Blue);

		return true;
	}

	/**Lässt immer abwechselnd die beiden Spieler spielen. Dabei wird immer die 
	*Reihenfolge request confirm update eingehalten. Es wird solange gespielt bis ein
	*illegaler Move gemacht wurde oder einer der beiden gewonnen hat.
	*@throws RemoteException
	*		wenn bei der Kommunikation mit dem Netzwerk ein Fehler auftritt
	*@throws Exception
	*		wenn beim Spielen ein Fehler auftritt*/
	private void play() throws Exception, RemoteException{
		while(board.getStatus() == Status.Ok) {
			Move m = currPlayer.request();
			board.make(m);
			currPlayer.confirm(board.getStatus());
			oppPlayer.update(m, board.getStatus());

			swapPlayer = currPlayer;
			currPlayer = oppPlayer;
			oppPlayer = swapPlayer;

			gui.printBoard();
			Thread.sleep(delay);
		}
	}

	/**Erstelle eine neue grafische Ausgabe von einem {@link nowhere2gopp.preset.Viewer} 
	*eines Boards
	*@param viewer
	*		{@link nowhere2gopp.preset.Viewer} der die nötigen Informationen zur Verfügung stellt
	*/
	private void setGui(Viewer viewer){
		gui = new GUI((BoardViewer) viewer);
	}
}









