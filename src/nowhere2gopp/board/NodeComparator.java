package nowhere2gopp.board;

import java.util.*;
import nowhere2gopp.preset.Site;

/**
 * Die Klasse ermöglicht den Bau eines {@link Comparator}, 
 * der genutzt wird um den zu besuchenden Knoten in der Warteschlange 
 * vom Typ {@link PriorityQueue} eine Priorität zuzuordnen. 
 * Dafür wird dem Konstruktor eine {@link NodeComparator#target}-{@link Site}
 * übergeben, deren Abstand zum jeweiligen Knoten 
 * die Priorität in der 
 * <a href="https://docs.oracle.com/javase/8/docs/api/java/util/PriorityQueue.html">
 * PriorityQueue</a> 
 * bestimmt.
 * Dieser Abstand wird mit der Funktion {@link #getDistance(Site start)} bestimmt.
 */
public class NodeComparator implements Comparator<Node>{
	/**Ziel-{@link Site} des Komparators*/
	Site target;
	/**Dem Konstruktor wird eine {@link Site} target
 	 * übergeben, deren Abstand zum jeweiligen Knoten 
 	 * die Priorität in einer Liste z.B. einer <a href="https://docs.oracle.com/javase/8/docs/api/java/util/PriorityQueue.html">
 	 * PriorityQueue</a> bestimmt; dabei werden kleinere Werte in einer <a href="https://docs.oracle.com/javase/8/docs/api/java/util/PriorityQueue.html">
 	 * PriorityQueue</a> früher ausgewertet.
 	 * @param target {@link Site} zu der der Abstand bestimmt wird.
 	 */
	public NodeComparator( Site target ){
 		this.target = target;
	}
	/**
	 * Zwei Nodes werden bezüglich des Abstandes ihrer {@link Site} zur {@link NodeComparator#target}-{@link Site} verglichen.
	 * @param a Erste Node
	 * @param b Zweite Node
	 * @return Vergleichswert der Knoten bezüglich des Abstandes ihrer Site zur target-Site
	 */
	@Override
	public int compare( Node a, Node b ){
		return getDistance( a.getSite() ) - getDistance( b.getSite() );
	}
	/**
	 * Überlagerte Version von {@link #getDistance( Site start, Site end)} bei der als Referenz 
	 * automatisch die {@link NodeComparator#target}-{@link Site} verwendet wird.
	 * @param start Startsite
	 * @return Distanz zwischen start- und target-Site
	 */
	int getDistance( Site start ){
		return getDistance( start, target );
	}
	/** 
	 * Bestimmt den Abstand zweier Sites.
	 * @param start Startsite
	 * @param end Endsite 
	 * @return Distanz der Sites in den Knoten auf dem Spielbrett
	 */
	static int getDistance( Site start, Site end ){

		int dx = end.getColumn() - start.getColumn();
		int dy = end.getRow() - start.getRow();

		return Math.abs( dx ) + Math.abs( dy ) + Math.abs( dx + dy);
	}
}