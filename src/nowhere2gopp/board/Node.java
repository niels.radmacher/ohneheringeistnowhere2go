package nowhere2gopp.board;

import nowhere2gopp.preset.Site;

/**Klasse, die Knoteneigenschaften liefert*/
public class Node{
	/**{@link Site} des Knotens*/
	private Site site;
	/**Liste der direkten Nachbarn des Knotens*/
	private Node[] neighbors = new Node[6];
	/**Wurde der Knoten besucht?*/
	boolean visited;
	/**Befindet sich ein Agent auf der {@link Site} dieses Knotens?*/
	boolean occupied;

	/**
	 * Erzeugt einen Knoten der als {@link Site} den Parameter site
	 * einspeichert. Zudem gibt es ein Feld
	 * neighbors in dem bis zu sechs potentielle Nachbarknoten vom Typ {@link Node} gespeichert werden können.
	 * @param site {@link Site} die eingespeichert wird.
	 */
	public Node (Site site){

		this.site = new Site( site.getColumn(), site.getRow() );
		this.visited = false;
		this.occupied = false;
     
		for(int i = 0; i < 6; i++)
			neighbors[i] = null;
		
	}
	/*Soll nur noch in Klasse Board genutzt werden*/
	/**
	 * Liefert die {@link Site}, die ein Knoten {@link Node} trägt.
	 * @return Site die im Knoten gespeichert ist
	 */
	Site getSite(){
		return site;
	}
	/*Soll nur noch in Klasse Board genutzt werden*/
	/**
	 * Liefert den Zeiger auf das Feld zurück, das die Nachbarn enthält, 
	 * die ein Knoten vom Typ {@link Node} hat.
	 * @return Liste mit direkten Nachbarn des Knotens
	 */
	Node[] getNeighbors(){
		return neighbors;
	}
	
	/**
	 * In vorgegebener Richtung wird eine Verbindung zu einem weiteren Knoten node gebaut. 
	 * Dabei ist die Richtung gegeben durch den Vektor vom aufrufenden Knoten zum gerufenen Knoten node,
	 * in diskreten Werten von 0 bis 5 für 60 Grad-Schritte mathematisch positiv gezählt,
	 * wobei ein rechter Nachbar den Wert 0 für die Richtung hat. Stichwort Polar-Koordinaten
	 * @param node Zu verknüpfender Knoten
	 * @param direction Richtung in der verknüpft werden soll
	 * @throws IllegalArgumentException wenn {@code direction < 0 || direction > 5}
	 * @throws NullPointerException wenn {@code node==null}.
	 */
	void setBridge( Node node, int direction )throws IllegalArgumentException{	//Package private

		if(direction < 0 || direction > 5){
			throw new IllegalArgumentException( 
				"Für die Richtung sind nur Werte von 0 bis 6 zulässig" );
		}
		
		if(node == null)
			throw new NullPointerException("Zweiter Knoten ist null!");
		
		/**Erlaubt prinzipiell das Verbinden beliebiger Knoten, auch wenn diese 
		eigentlich keine Nachbarn sind!
		Darf daher nur mit Bedacht beim Intialisieren des Boards verwedet weden. 
		Darum Package private*/

		this.neighbors[direction] = node;
		node.neighbors[ (direction + 3) % 6 ] = this;
		/*(direction + 3) % 6 ist die entgegengesetzte Richtung*/

	}	
	/** 
	 * Analog zu setLink wird nun eine Verbindung zu einem Nachbarknoten abgerissen.
	 * @param node Zu trennender Knoten
	 * @param direction Richtung in der getrennt werden soll
	 * @throws IllegalArgumentException wenn {@code direction < 0 || direction > 5}
	 * oder {@code neighbors[direction] != node} oder 
	 * {@code node.neighbors[ (direction + 3) % 6 ] != this}
	 */
	void removeBridge( Node node, int direction ) throws IllegalArgumentException{
		if(direction < 0 || direction > 5){
			throw new IllegalArgumentException( 
				"Für die Richtung sind nur Werte von 0 bis 5 zulässig" );
		}
		if(neighbors[direction] == node && node.neighbors[ (direction + 3) % 6 ] == this){
			neighbors[direction] = null;
			node.neighbors[ (direction + 3) % 6 ] = null;
		}
		/*Es sollen die richtigen Zeiger übergeben werden. Eine Äquivalenz im Sinne einer equals-Methode reicht nicht.*/
		else {
			if (neighbors[direction] != node) 
				throw new IllegalArgumentException( "Der zweite Knoten " + node.getSite() + 
					" ist nicht in dieser Richtung als Nachbar des ersten Knotens " + 
					this.getSite() +  "eingetragen" );
		
		 	if ( node.neighbors[ (direction + 3) % 6 ] != this ) 
				throw new IllegalArgumentException( "Der erste Knoten " + this.getSite() + 
					" ist nicht in dieser Richtung als Nachbar des zweiten Knotens " + 
					node.getSite() +  "eingetragen" );
		}

	}
	/** 
	 * Ist wahr, wenn das Argument 
	 * node in beliebiger Richtung als Nachbar eingetragen ist 
	 * und der rufende Knoten in umgekehrter Richtung als Nachbar des Knotens
	 * eingetragen ist.
	 * @param node Knoten der auf die Nachbarschaft geprüft wird.
	 * @throws IllegalStateException wenn der aufrufende Knoten den Knoten node 
	 * zwar als Nachbarn eingetragen hat, jedoch nicht umgekehrt.
	 * @return Ist dieser Knoten mit dem Knoten node verbunden?
	 */
	public boolean isNeighbor(Node node) throws IllegalStateException{
		
		for( int i = 0; i < 6; i++){
			if( neighbors[i] == node ){
				if(node.neighbors[ (i + 3) % 6 ] == this )
					return true;
				else
					throw new IllegalStateException( 
						"Der zweite Knoten ist zwar als Nachbar von des Ersten eingetragen, " +
						"aber der Zweite ist nicht als Nachbar des ERsten eingetragen");
			}
		}
		return false;
	}
}
