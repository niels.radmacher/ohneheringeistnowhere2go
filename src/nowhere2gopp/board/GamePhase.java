package nowhere2gopp.board;

import java.io.Serializable;

/**
 * Enumeration der verschiedenen Spielphasen 
 */
public enum GamePhase implements Serializable {
	/**Erste Spielphase: Zwei Links werden getrennt.*/
	LinkLink,
	/**Zweite Spielphase: Der Agent des Spielers wird auf das Spielfeld gesetzt, bewegt und danach ein Link getrennt.*/
	SetAgentLink,
	/**Dritte Spielphase: Der Agent des Spielers wird bewegt und dann ein Link getrennt.*/
	AgentLink
}
