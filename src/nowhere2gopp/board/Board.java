package nowhere2gopp.board;

import nowhere2gopp.preset.*;
import nowhere2gopp.io.*;
import java.util.*;

/**Klasse, die das Board erstellt*/
public class Board implements Viewable, Playable{ 
	/**Größe des Spielfeldes*/
	private final int size;
	/**Liste der vorhandenen Links*/
	private ArrayList <SiteSet> links;
	/**Liste der entfernten Links*/
	private ArrayList <SiteSet> removedLinks;
	/**Knotengitter, das die hexagonale Struktur erhält sowie Informationen 
	 * über die Nachbarschaft der Knoten vom typ {@link Node} enthält
	 */
	private final ArrayList < ArrayList <Node> > nodes;
	/**Status des Spielfeldes*/
	private Status status;
	/**Position des roten Agenten*/
	private Site redPos; 
	/**Position des blauen Agenten*/
	private Site bluePos;
	/**Gesamtzahl der Nodes (auch die ohne links)*/
	private int nodeNumber;	
	/**Nummer des aktuellen Zuges*/
	private int turnNumber;
	/**In dieser Runde werden die Agenten aufs Feld gesetzt.*/ 
	private final int phase2;
	/**Ist dieses Board original?*/
	private boolean original;
	/*Für die AI sind Kopien des Spielbretts interessant, auf denen hypothetische 
	Züge gemacht und wieder zurückgenommen werden können. Solche Operationen sind 
	im tatsächslichen Spielverlauf jedoch untersagt.*/	

	/**
	 * Konstruktor für ein neues Spielfeld ungerader Größe zwischen drei und elf
	 * @param size Größe des zu erstellenden Spielfeldes
	 * @throws IllegalArgumentException Gerade Spielfeldgröße
	 */
	public Board( int size) throws IllegalArgumentException{
		if( size % 2 == 0)
			throw new IllegalArgumentException(
				"Die Spielfeldgröße muss eine ungerade Zahl sein");
		this.size = size;
		redPos = null;
		bluePos = null;
		nodes = new ArrayList < ArrayList <Node> > ();
		links = new ArrayList<SiteSet>();
		removedLinks = new ArrayList<SiteSet>();
		nodeNumber = 0;
		initializeNodes();
		status = Status.Ok;
		phase2 = power(2,(size-3)/2) + 1; 
		turnNumber = 1; 
		/*Das Spiel beginnt im ersten Zug. 
		TurnNumber gibt immer die Nummer des kktuellen Zuges an.*/

		original = true;
	}
	
	/**
	 * Konstruktor für die Kopie eines Spielbrettes
	 * @param board Originalspielbrett, das bis auf den boolean 
	 * {@link Board#original} vollständig kopiert wird.
	 */
	public Board( Board board  ){

		this( board.getSize() );

		for(SiteSet ss : board.getRemovedLinks()){
			removeLink(ss);
		}
		redPos = board.redPos;
		getNode(redPos).occupied = true;

		bluePos = board.bluePos;
		getNode(bluePos).occupied = true;

		links = board.getLinks();
		removedLinks = board.getRemovedLinks();

		turnNumber = board.getTurnNumber();
		status = board.getStatus();

		original = false;
	}
	/**
	 * Funktion zum Klonen eines Spielbrettes. Benutzt den Konstruktor {@link #Board( Board board)}.
	 * @return Vollständige Kopie des Spielbrettes bis auf {@link Board#original}
	 */
	@Override
	public Board clone(){
		return new Board(this);
	}
	/**Funktion zum Initialisieren des Knotengitters {@link Board#nodes} und Verknüpfen der Knoten*/
	private void initializeNodes(){
		
		ArrayList <Node> column;
		for( int i = 0; i < size; i++){
			column = new ArrayList <Node>();
			nodes.add(column);	

			for( int j = 0; j < size; j++){

				if ( isOnBoard(i,j) ){
					Node tmp = new Node( new Site(i,j) );
					nodeNumber ++;
					column.add( tmp );
					SiteSet siteSet;
					/*Set Links*/					
					if( isOnBoard(i-1, j) ){
						/*Verknüpfung der Nodes*/
						tmp.setBridge(nodes.get(i-1).get(j), 3);	
						/*Aufnahme der Verknüpfung in die Collection "links"*/ 					   		   
						links.add( new SiteSet( nodes.get(i-1).get(j).getSite(), tmp.getSite()) );
					}
					if( isOnBoard(i-1, j-1) ){
						tmp.setBridge( nodes.get(i-1).get(j-1), 4);
						links.add( new SiteSet( nodes.get(i-1).get(j-1).getSite(), tmp.getSite()) );
					}
					if( isOnBoard(i, j-1) ){
						tmp.setBridge( nodes.get(i).get(j-1), 5);
						links.add( new SiteSet( nodes.get(i).get(j-1).getSite(), tmp.getSite()) ); 
					}

				}
				else
					column.add( null );
			}
		}
	}
	/**
	 * Ist wahr, wenn eine Node mit den Koordinaten x und y Teil des Spielfeldes ist.
	 * @param x x Koordinate
	 * @param y y Koordinate
	 * @return Ist die Site {x,y} auf dem Spielbrett?
	 */
	public boolean isOnBoard( int x, int y){
		int edge = (size + 1) / 2;
		return ( x < edge || x < y + edge ) && ( y < edge || y < x + edge ) 
			&& x >= 0 && y >= 0 && x < size && y < size;
	}
	/**
	 * Gibt die Site zurück, die an der Position (x,y) im Knotengitter gespeichert ist. 
	 * @param x x Koordinate
	 * @param y y Koordinate
	 * @return Site an der Position {x,y}
	 */
	public Site getSite( int x, int y){
		return nodes.get(x).get(y).getSite();
	}

	/**
	 * Gibt den Knoten zurück, der die übergebene Site enthält.
	 * @param site Übergebene Site
	 * @return Node die die Site site trägt
	 */
	public Node getNode( Site site){
		return nodes.get(site.getColumn() ).get(site.getRow() );
	}
	
	/**
	 * Funktion zum Ausführen eines Spielzuges auf dem Spielbrett
	 * @param move Auszuführender Zug
	 * @throws IllegalStateException Wenn Boardstatus nicht ok, playercolor nicht 
	 * definiert oder {@link #isLegalMove(Move move, PlayerColor playerColor)} nicht wahr ist
	 */
	@Override
    public void make(final Move move) throws IllegalStateException{
    	/*Wenn ein Zug in der aktuellen Spielphase nicht erlaubt ist, 
    	so gibt es eine MoveFormatException*/

    	if(status != Status.Ok){
    		throw new IllegalStateException("Boardstatus nicht ok.");
    	}
    	MoveType moveType = move.getType();
    	PlayerColor playerColor = getTurn();

    	try{
    		if(isLegalMove(move, playerColor)){
				switch(moveType){

					case LinkLink:
				    	removeLink( move.getOneLink() );
				    	removeLink( move.getOtherLink() );
			    		/*Das Spiel ist so konstruiert, dass es in der ersten Spielphase 
			    		nicht entschieden werden kann.*/
			    		break;

			    	case AgentLink:
						/*Überschreiben des Agenten des Spieles der noch am Zug ist, 
						mit seiner neuen Position agent*/
						setAgent( move.getAgent().getSecond() );

			    		/*Entfernen des Links*/
			    		removeLink( move.getLink() );

						switch (playerColor) {
							case Red:
								if(!canMove(PlayerColor.Blue))
									status = Status.RedWin;
							break;
							case Blue:
								if(!canMove(PlayerColor.Red))
									status = Status.BlueWin;
						}
						/*Es gibt keine Situation, in der sich nur noch eine Brücke auf dem Spielfeld befindet, 
						sich aber beide Spieler noch bewegen können. 
						Somit entscheidet allein die Bewegungsfreiheit des Agenten des nächsten 
						Spielers über dessen Niederlage*/
						break;

			    	case Surrender:
			    		switch(playerColor){
			    			case Red:
			    				status = Status.BlueWin;
			    				break;
			    			case Blue: 
			    				status = Status.RedWin;
			    				break;
			    			default: 
			    				throw new IllegalStateException(
			    					"Surrender: playerColor ist weder rot noch blau");
			    		}
			    		break; 

			    	case End: 
			    		break;	
				}
			}
		}catch (Exception e) {
			throw new IllegalStateException(e.getMessage());
		}
    	/*Der Spielzug ist beendet und der andere Spieler darf seinen Zug machen*/
    	turnNumber ++;
    }
    /**
     * Kehrt einen gemachten (legalen) Zug wieder um und sollte ausschließlich 
     * sofort nach einem solchen ausgeführt werden.
     * Es ist nicht erlaubt im Spiel Züge zurück zu nehmen, aber erlaubt, 
     * mögliche Verläufe zu simulieren und diese hypothetischen Züge rückgängig zu machen.
     * @param move Zurückzunehmender Zug
     * @throws UnsupportedOperationException Wenn nicht {@link Board#original}
     */
    public void reverse(final Move move) throws UnsupportedOperationException{

    	if( original )
    		throw new UnsupportedOperationException(
    			"Auf originalen Spielfeldern können Schritte nicht zurück genommen werden.");

    	turnNumber --;

    	switch( move.getType() ){

    		case LinkLink:
    			setLink( move.getOneLink() );
		    	setLink( move.getOtherLink() );
	    
	    	case AgentLink:
 				setLink( move.getLink() );
	    		setAgent( move.getAgent().getFirst() );
    	}

    	status = Status.Ok;
    }

    /*
    private void setIllegal(){
    	status = Status.Illegal;
    }
    */

    /**
	 * Liefert den Wahrheitswert wahr, falls der Zug den Spielregeln entspricht. 
	 * Ansonsten wird eine Exception geworfen.
	 * @param move Zu prüfender Zug
	 * @param playerColor Farbe des Spielers der diesen Zug ausführen möchte
	 * @throws IllegalStateException Wenn ein Zug nicht legal ist
	 * @return Ist der Zug legal?
     */
    public boolean isLegalMove(Move move, PlayerColor playerColor) throws IllegalStateException{
    	switch (move.getType()) {
    		case LinkLink:
				/*Phase 1*/
				if( getGamePhase() != GamePhase.LinkLink ){					
					throw new IllegalStateException(
						"LinkLink ist nur in der ersten Spielphase ein legaler Zug.");
				}
				if( links.contains( move.getOneLink() ) && links.contains( move.getOtherLink() ) 
					&& !move.getOneLink().equals(move.getOtherLink()) ){
					return true;
				}
				else{
					//setIllegal();				
					throw new IllegalStateException(
						"Beide Links müssen noch intakt und zudem unterschiedlich sein.");
				}
			case AgentLink:

				if( getGamePhase() == GamePhase.LinkLink ){
					//setIllegal();
    			    throw new MoveFormatException(
	    				"AgentLink ist nur in der zweiten und dritten "
	    				 + "Spielphase ein legaler Zug.");
	    		}
	    		/*getAgent(playerColor) aus Board*/
	    		Site agent = getAgent(playerColor); 
	    		/*getAgent() aus preset.move returns SiteTuple*/		
	    		Site first = move.getAgent().getFirst();	
	    		Site second = move.getAgent().getSecond();

	    		/*Phase 2*/
	    		if( getGamePhase() == GamePhase.SetAgentLink &&
	    			!isOnBoard( first.getColumn(), first.getRow())){
    				//setIllegal(); 
    				throw new IllegalStateException(
    					"Phase 2: Die erste Site im Move ist nicht Teil des Spielfeldes.");
	    		}

	    		/*Phase 3*/
				if( getGamePhase() == GamePhase.AgentLink && !first.equals(agent)){
	    			//setIllegal(); 
    				throw new IllegalStateException(
    					"Phase 3: Die erste Site im Move stimmt nicht mit der Position des Agenten überein.");
	    			
	    		}
				
    			if( isOnBoard(second.getColumn(), second.getRow())
    				&& isPath(first, second) ){
    				if( links.contains( move.getLink() ) )
    					/*Den Agenten bewegen*/
						return true;
					else {
						//setIllegal();
    					throw new IllegalStateException(
							"Agent: Der Link wurde bereits entfernt.");
					}
				}
				else {
					//setIllegal();					
    				throw new IllegalStateException(
    					"Die zweite Site" + second + " ist von " + first + "nicht erreichbar." + 
    					" Gegner steht auf. "+ getOpponent(playerColor));
    			}
    		case Surrender:
    			return true;
    		case End:
    			return true;
    		default:
    			throw new MoveFormatException("MoveType konnte nicht erkannt werden");
    	}
    }
    /** 
     * Setzt einen Link zwischen zwei Knoten und ihren Sites.
     * @param siteSet Zu setzender Link
     */
    private void setLink(SiteSet siteSet){

    	Node first = getNode(siteSet.getFirst());
    	Node second = getNode(siteSet.getSecond());

    	first.setBridge( second, direction(siteSet.getFirst(), siteSet.getSecond()) );
    	removedLinks.remove(siteSet);
    	links.add(siteSet);
    }
    /** 
     * Entfernt einen Link zwischen zwei Knoten und ihren Sites.
     * @param siteSet Zu entfernender Link
     * @throws IllegalStateException Wenn der Link nicht (mehr) existiert
     */
    private void removeLink( SiteSet siteSet) throws IllegalStateException{

		Node first = getNode(siteSet.getFirst());
		Node second = getNode(siteSet.getSecond());

		try{
	    	/*Beseitigen des links in den Knoten*/
			first.removeBridge( second, direction(siteSet.getFirst(), siteSet.getSecond()) );
			/*Entfernen des Links aus der Collection*/
	    	links.remove( siteSet ); 
	    	removedLinks.add( siteSet );
		}catch (IllegalArgumentException e) {
			throw new IllegalStateException("Der Link existiert nicht (mehr).");
		}
    }
    /**
     * Gibt die Richtung des Vektors von der Site a zur benachbarten Site b an, 
     * wobei die Richtungskonvention aus der Klasse {@link Node} benutzt wird.
     * @param a Erste Site
     * @param b  Zweite Site
     * @throws IllegalArgumentException Wenn die Knoten keine direkten Nachbarn sind
     * @return Wert für die Richtung von a nach b gemäß Konvention der Klasse {@link Node}
     */
    public int direction( Site a, Site b ){
	 	if(b.getColumn() - a.getColumn() == -1){
			if( b.getRow() - a.getRow() == -1)
				return 4;
			else if( b.getRow() - a.getRow() == 0)
				return 3;
		}
	 	else if(b.getColumn() - a.getColumn() == 0){
			if( b.getRow() - a.getRow() == -1)
				return 5;
			else if( b.getRow() - a.getRow() == 1)
				return 2;
		}
		else if(b.getColumn() - a.getColumn() == 1){
			if( b.getRow() - a.getRow() == 0)
				return 0;
			else if( b.getRow() - a.getRow() == 1)
				return 1;
		}
        throw new IllegalArgumentException(
        	"Die Nodes sind keine direkten Nachbarn");
		}

	
	/**
	 * Liefert eine ArrayList vom Typ Site zurück, in der alle 
	 * von der Site start erreichbaren Sites enthalten sind. 
	 * Dabei wird berücksichtigt, welche links noch erhalten sind, sowie, 
	 * dass die vom Gegner okkupierte Site nicht genutzt werden kann.
	 * @param start Site von der aus mit der Funktion {@link #BFS( Site start, Site start)} 
	 * alle erreichbaren Sites in der Zusammenhangskomponente bestimmt werden.
	 * @return Liste aller von der Site start erreichbaren Sites
	 */
	public ArrayList<Site> accessibleSites( Site start ){
		return BFS( start , start ); 
		/*Die Site start kann aufgrund der Implementation von BFS nicht gefunden werden.
		Es wird daher eine Liste aller besuchten Sites zurückgegeben*/
	}

	/**
	 * Liefert eine Liste aller intakten Links zurück, die sich in der 
	 * gleichen Zusammenhangskomponente wie start befinden.
	 * @param start Site, die bestimmt, von welcher Zusammenhangskomponente 
	 * die Links zurückgegeben werden
	 * @return Liste aller intakten Links, die sich in der 
	 * gleichen Zusammenhangskomponente wie start befinden
	 */
	public ArrayList<SiteSet> accessibleLinks( Site start){
		ArrayList<Site> accSites = accessibleSites(start);
		ArrayList<SiteSet> allLinks = getLinks();
		ArrayList<SiteSet> accLinks = new ArrayList<SiteSet>();

		for( SiteSet ss : allLinks ){
			if( accSites.contains( ss.getFirst() ) )
				accLinks.add(ss);
		}
		return accLinks;
	}
	/**
	 * Gibt an, ob der Spieler, der gerade am Zug ist, 
	 * seinen Agent von der Site start zur site end bewegen darf. 
	 * Es wird dabei nicht geprüft, ob sich sein Agent auf start befindet.
	 * Dabei wird berücksichtigt, welche links noch erhalten sind, sowie, 
	 * dass die vom Gegner okkupierte Site nicht genutzt werden kann.
	 * @param start Start-Site der Breitensuche
	 * @param end Ein Fund der Site end beendet die Breitensuche.
	 * @return Ist wahr, wenn es einen legalen Weg von der Site start zur Site end gibt. 
	 * Dies ist der Fall wenn {@link #BFS( Site start, Site end )} eine Liste der Länge eins übergibt, 
	 * in der sich lediglich die Site end befindet. 
	 */
	public boolean isPath( Site start, Site end ){
		ArrayList<Site> notes = BFS( start, end );
		return  notes.size() == 1 && notes.get(0).equals( end );
		/*Falls die site end von Start erreicht werden kann, so findet BFS sie und
		 schickt statt der Liste mit allen besuchten Sites eine Arraylist,
		 die als Element nur die Site end trägt.*/
	}	

	/**
	* Angepasste breadth-first search mit <a href="https://docs.oracle.com/javase/8/docs/api/java/util/PriorityQueue.html">
 	* PriorityQueue</a>
 	* vom typ {@link Node}, sodass die Site end möglichst schnell gefunden wird, 
 	* da die Knoten deren Sites näher and end sind, zuerst ausgewertet werden.
 	* @param start Start-Site der Breitensuche
	* @param end Ein Fund der Site end beendet die Breitensuche.
 	* @return Wird die Site end nicht gefunden, so wird eine Liste mit allen besuchten Sites übergeben. 
 	* Wenn nicht, so wird nur die Endsite in einer Liste der Länge eins übergeben. 
 	* Dies wird von der Funktion {@link #isPath( Site start, Site end )} richtig interpretiert.
	*/
	public ArrayList<Site> BFS( Site start, Site end ){ 

		ArrayList<Site> accessibleSites = new ArrayList<Site>( getNodeNumber() - 1 );
		/*Im besten Fall stehen für den (roten) Spieler alle Sites/Nodes zur Verfügung, auf der er nicht steht.
		Angabe der Feldgröße vermindert umspeichern.*/
		
		PriorityQueue<Node> queue = new PriorityQueue<Node>( new NodeComparator( end ) );
		Node anfang = getNode(start);
		/*anfang trägt die Site start. Nach den Spielregeln kann sie nicht 
		Anfang und Ende eines Moves sein.
		Daher gehört start nicht zu den erreichbaren Sites*/

		queue.add( anfang );  
		anfang.visited = true; 
		Node node;
		boolean found = false;
		while( !queue.isEmpty() && !found ){
			node = queue.poll();
			for( int i = 0; i < 6; i++ ){
				Node neighbor = node.getNeighbors()[i];

				if( neighbor != null && !neighbor.occupied  
					&& !neighbor.visited ){
					/*Der Besuch wird direkt in der Node hinterlegt*/
					neighbor.visited = true;
					/*Nachbarn werden zur Queue hinzugefügt*/
					queue.add(neighbor);
					/*Alle besuchten Sites (außer start) werden der Liste hinzugefügt.*/
					accessibleSites.add( neighbor.getSite() );

					if( neighbor.getSite().equals( end ) )
						found = true;
				}
			}
		}
		/*Die Information über den Besuch der Nodes wird zurück gesetzt*/
		for(int i = 0; i < size; i++){
			for(int j = 0; j < size; j++){
				if(isOnBoard( i, j) )
					nodes.get(i).get(j).visited = false;
			}
		}
			
		/*Falls die gesuchte Site end von start erreichbar ist, 
		so wird nur end übergeben*/
		if( found ){
			accessibleSites = new ArrayList<Site>();
			accessibleSites.add( end ); 
		}
		/*Ansonsten werden alle besuchten Sites übergeben*/
		return accessibleSites;

	}
	/**
	 * Liefert einen {@link BoardViewer} für die graphische Ausgabe.
	 * @return {@link BoardViewer} für die graphische Ausgabe. 
	 */
	@Override
	public Viewer viewer(){
		return new BoardViewer(this);
	}

	/**
	 * Erzeugt aus einer Site start und einer Liste von Sites eine Liste vom typ {@link SiteTuple}.
	 * @param start Startsite
	 * @param endSites Liste von end-Sites
	 * @return Liste vom typ {@link SiteTuple} von start zu jeder Site in der Liste endSites
	 */
	public ArrayList<SiteTuple> siteToTuple( Site start, ArrayList<Site> endSites ){
		ArrayList<SiteTuple> links = new ArrayList<SiteTuple>( endSites.size() );
		for( int i = 0; i < endSites.size(); i++){
			links.add( new SiteTuple( start, endSites.get(i) ) );
		}
		return links;
	}

	/**
	 * Erzeugt aus einer Site start und einer Liste von Sites eine Liste vom typ {@link SiteSet}.
	 * @param start Startsite
	 * @param endSites Liste von end-Sites
	 * @return Liste vom typ {@link SiteSet} von start zu jeder Site in der Liste endSites
	 */
	public ArrayList<SiteSet> siteToSet( Site start, ArrayList<Site> endSites ){
		ArrayList<SiteSet> links = new ArrayList<SiteSet>( endSites.size() );
		for( int i = 0; i < endSites.size(); i++){
			links.add( new SiteSet( start, endSites.get(i) ) );
		}
		return links;
	}
	/**
	 * Bestimmt aus der {@link Board#turnNumber} wessen Zug drann ist.
	 * @return Farbe des Spielers, der am Zug ist 
	 */
	public PlayerColor getTurn(){
		return (turnNumber-1)%2==0 ? PlayerColor.Red : PlayerColor.Blue;
	}
	/**
	 * Bestimmt aus der Rundennummer {@link #getRoundNumber()} und der 
	 * Objektvariable {@link Board#phase2} die aktuelle Spielphase.
	 * @return Aktuelle Spielphase
	 */
	public GamePhase getGamePhase(){
		if( getRoundNumber() < phase2 )
			return GamePhase.LinkLink;
		else if ( getRoundNumber() == phase2 )
			return GamePhase.SetAgentLink;
		else
			return GamePhase.AgentLink;
	}
	/**
	 * Bestimmt aus der {@link Board#turnNumber} die Rundennummer.
	 * @return Rundennummer
	 */
	public int getRoundNumber(){
		return (turnNumber+1)/2;
	}
	/**
	 * Liefert die Runde der zweiten Spielphase zurück.
	 * @return Wert von {@link Board#phase2}
	 */
	public int getPhase2(){
		return phase2;
	}
	/**
	 * Liefert die Spielfeldgröße zurück.
	 * @return  Wert von {@link Board#size}
	 */
	public int getSize(){
		return size;
	}
	/**
	 * Liefert den Status des Spielbrettes zurück.
	 * @return Wert von {@link Board#status}
	 */
	public Status getStatus(){
		return status;
	}
	/**
	 * Liefert den Agenten des Spielers der gerade am Zug ist.
	 * @return Agent des Spielers der gerade am Zug ist
	 */
    public Site getAgent(){
    	return getAgent( getTurn() );
    }
    /**
     * Liefert den Agenten des Spielers der Farbe color.
     * @param color Farbe des Spieles dessen Agent zurückgeliefert werden soll
     * @throws IllegalStateException Falls die color weder rot noch blau ist.
     * @return Agent des Spielers der Farbe color
     */
    public Site getAgent(final PlayerColor color) throws IllegalStateException{
    	switch(color){
			case Red:
				return redPos;
			case Blue:
				return bluePos;
			default:
				throw new IllegalStateException("Spielerfarbe die getAgent()"+
					" übergeben wurde ist werder rot noch blau");
		}
    }

    /**
     * Liefert den Agenten des Spielers der gerade nicht am Zug ist.
     * @return Agent des Spielers der gerade nicht am Zug ist
     */
	public Site getOpponent(){
		return getOpponent( getTurn() );
	}

	/**
     * Liefert den Agenten des Gegners vom Spieler der Farbe color.
     * @param color Farbe des Spielers
     * @throws IllegalStateException Falls die color weder rot noch blau ist.
     * @return Agent des Gegners vom Spieler der Farbe color
     */
	public Site getOpponent(final PlayerColor color) throws IllegalStateException{
    	switch(color){
			case Red:
				return bluePos;
			case Blue:
				return redPos;
			default:
				throw new IllegalStateException("Spielerfarbe die getAgent()"+
					" übergeben wurde ist werder rot noch blau");
		}
    }

    /**
     * Überschreibt die Position des Agenten des Spielers der gerade am Zug ist.
     * @param newPos Site mit der neuen Position des Agenten
     * @throws IllegalStateException Wenn der Agent des Spielers, der am Zug ist, 
     * sich in der dritten Spielphase nicht auf dem Spielfeld befindet oder die Spielerfarbe nicht definiert ist.
     */
    private void setAgent(Site newPos) throws IllegalStateException{
    	PlayerColor playerColor = getTurn();
    	Site agent = getAgent(playerColor);
    	
    	try{
    		getNode(agent).occupied = false;
    	}
    	catch(NullPointerException e){
			if( getGamePhase() != GamePhase.SetAgentLink )
				throw new IllegalStateException(
					"Der Agent der Farbe" + playerColor + "befand sich nicht auf dem Spielfeld sondern auf "
					 + agent);
		}

		agent = newPos;
		getNode(agent).occupied = true;

    	switch(playerColor){
    		case Red:
				redPos = agent;
				break;
			case Blue:
				bluePos = agent;
				break;
			default:
				throw new IllegalStateException("Spielerfarbe dieses Objekts"+
					" wurde nicht initialisiert");
		}
    }

    /**
     * Überprüft ob ein Spieler noch Nachbarn hat, die er betreten kann.
     * @param playerColor Farbe des zu überprüfenden Spielers
     * @return Hat der Agent des Spieler der am Zug ist Nachbarn die er betreten kann?
     */
    private boolean canMove(final PlayerColor playerColor){
    	Node node;
    	switch(playerColor){
			case Red:
				node = getNode(redPos);						
				break;
			case Blue:
				/*Erste Bewegung des roten Agenten muss möglich sein*/
				if(bluePos == null)
					return true;
				node = getNode(bluePos);
				break;
			default:
				System.err.println(
					"canMove: playerColor ist weder rot noch blau");
				return false;
		}
		for (int i = 0; i < 6 ; i++ ) {
			/*Es muss eine Brücke bestehen an deren Ende nicht der gegnerische Agent steht.*/
			Node neighbor = node.getNeighbors()[i];
			if( neighbor != null && !neighbor.getSite().equals(getOpponent(playerColor)) )
				return true;
		}
		return false;
    }
    /**
     * Liefert die verbleibenden direkten Nachbarn einer Site site. 
     * Mögliche indirekte Verbindungen mittels weiterer Sites werden NICHT erkannt.
     * @param site Site die auf direkte Nachbarn geprüft wird
     * @throws IllegalArgumentException Wenn site nicht Teil des Spielfeldes ist.
     * @return Liste der verbleibenden direkten Nachbarn einer Site site
     */
  	public ArrayList<Site> getNeighborSites( Site site ){

  		if( !isOnBoard( site.getColumn(), site.getRow()) )
  			throw new IllegalArgumentException(
  				"Die Site " + site+ " ist nicht Teil des Spielfeldes." );

		ArrayList<Site> neighbors = new ArrayList<Site>();
		Node node = getNode(site);

		Node[] neighborNodes = node.getNeighbors();
		if(neighborNodes == null){
			System.err.println("Die Nachbarn sind null");
		}
		for(int i = 0; i < 6; i++){
			if(neighborNodes[i] != null)
				neighbors.add( neighborNodes[i].getSite() );
		}

	return neighbors;
    }

    /**
     * Verbleibende direkte Verbindungen einer Site site zu ihren direkten Nachbarn. 
     * Mögliche indirekte Verbindungen mittels weiterer Sites werden NICHT erkannt.
     * @param site Site die auf direkte Nachbarn geprüft wird
     * @return Liste aller verbleibenden direkten Verbindungen einer 
     * Site site zu ihren direkten Nachbarn
     */
    public ArrayList<SiteSet> getNeighborLinks( Site site ){
		return siteToSet( site, getNeighborSites(site) );
    }

    /**
     * Liefert eine Liste aller intakten Links.
     * @return Liste aller intakten Links
     */
    public ArrayList<SiteSet> getLinks(){
    	return new ArrayList<SiteSet>(links);
    }
    /**
     * Liefert eine Liste aller entfernten Links.
     * @return Liste aller entfernten Links
     */
    public ArrayList<SiteSet> getRemovedLinks(){
    	return new ArrayList<SiteSet>(removedLinks);
    }
    /**
     * Liefert die Nummer des aktuellen Zuges.
     * @return Nummer des aktuellen Zuges
     */
    public int getTurnNumber(){
    	return turnNumber;
    }
    /**
     * Liefert die Gesamtzahl an Nodes eines Spielbrettes.
     * @return Gesamtzahl an Nodes eines Spielbrettes
     */
    public int getNodeNumber(){
    	return nodeNumber;
    }
    /**
     * Liefert den exakten Wert einer basis base zum Exponenten exp.
     * @param base Basis
     * @param exp Exponent
     * @return Wert einer basis base zum Exponenten exp
     */
    static int power(int base, int exp){
    	
    	int result = 1;
    	for(int i = exp; i>0; i--){
    		result *= base;
    	}
    	return result;
    }
}

	
