package nowhere2gopp.io;

import java.awt.Color;
import java.awt.geom.*;

import nowhere2gopp.preset.Site;


/** Definiert einen Zeichenbaren Knotenpunkt */
public class DrawableNode extends Ellipse2D.Double implements DrawableBoardElement
{

    /** mithilfe dieser BoardInfo klasse können alle nötigen Informationen über die Board Größe abgefragt werden,
     * so wie die Konversation von reihe/spalte auf Koordinate*/
    private final BoardInfo info;

    /** die Flächen Farbe des Nodes*/
    private Color fillColor = BoardColors.linkFill;

    /** die Rand Farbe des Elements */
    private Color borderColor = BoardColors.linkBorder;

    /** Enthält die nicht skalierte Position des Nodes*/
    private Point2D.Double p;

    /** Enthält den node */
    private Site site;

    /** Standard Konstruktor dieser Klasse 
     * @param site die site zu der ein zeichenbares Objekt erstellt werden soll
     * @param info liefert informationen über das spielbrett
     * @param p der Punkt an der die Site sitzt*/
    public DrawableNode(Site site, BoardInfo info, Point2D.Double p)
    {
        super(info.getScale()*p.getX()-info.getScale()/4 - info.getXoff(),info.getScale()*p.getY()-info.getScale()/4,info.getScale()/2,info.getScale()/2);
        this.info = info;
        this.site = site;
        this.p = p;
    }

    /**
     * @return the site
     */
    public Site getSite() {
        return site;
    }


    /** passt die Element größe der aktuellen frame größe an */
    @Override
    public void adaptElementSize() 
    {
        double scale = info.getScale();
        this.setFrame(scale*p.getX()-scale/4 - info.getXoff(),scale*p.getY()-scale/4,scale/2,scale/2);
    }

    /** @return die füll farbe dieser site*/
    @Override
    public Color getFillColor() {
        return fillColor;
    }

    /** @param newFillColor die neue Füllfarbe */
    @Override
    public void setFillColor(Color newFillColor) {
        fillColor = newFillColor;

    }

    /** @return die rand farbe der Site */
    @Override
    public Color getBorderColor() {
        return borderColor;
    }

    /** @param newBorderColor die neue Rand farbe */
    @Override
    public void setBorderColor(Color newBorderColor) {
        borderColor = newBorderColor;
    }
}
