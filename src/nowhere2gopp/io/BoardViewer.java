package nowhere2gopp.io;

import java.util.ArrayList;
import java.util.Collection;

import nowhere2gopp.board.Board;
import nowhere2gopp.board.GamePhase;
import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.Site;
import nowhere2gopp.preset.SiteSet;
import nowhere2gopp.preset.Status;
import nowhere2gopp.preset.Viewer;

/** Diese Klasse hat die Möglichkeit alle nötigen Informationen zu erfragen, die gebraucht
 * werden um das Spielbrett in der graphischen Ausgabe darzustellen. */
public class BoardViewer implements Viewer
{

    /** enthält das board von welchen informationen weitergeleitet werden sollen */
    private final Board board;

    /** standard konstruktor 
     * @param board das board auf das observiert werden soll*/
    public BoardViewer(Board board)
    {
        this.board = board;
    }

    /** @return die Playercolor des Spielers am zug zürück*/	
    @Override
    public PlayerColor getTurn() {
        return board.getTurn();
    }

    /** @return die Größe des Spielbrettes */
    @Override
    public int getSize() {
        int n = board.getSize();
        return n;
    }

    /** @return den Status des Boards */
    @Override
    public Status getStatus() {
        return board.getStatus();
    }

    /** @param color die farbe des Spielers für den die Position gefunden werden soll
     * @return die Position des Spielers */
    @Override
    public Site getAgent(PlayerColor color) {
        Site player = board.getAgent(color);
        return new Site(player.getColumn(), player.getRow());
    }

    /** @return die aktuell existierenden links*/
    @Override
    public Collection<SiteSet> getLinks() {
        /* WICHTIG: an dieser Stelle darf nicht die Orginale Collection übergeben werden 
         * sondern nur eine Kopie (so kann das Orginale Board nicht verändert werden) */
        ArrayList<SiteSet> returnLinks = new ArrayList<SiteSet>(board.getLinks());
        return returnLinks;
    }

    /** @return die Nummer der aktuellen Runden*/
    public int getRoundNumber()
    {
        return board.getRoundNumber();
    }

    /** @param start der startpunkt von dem die AccessibleSites gesucht werden sollen
     * @return alle Sites die vom Spieler besetzt werden können */
    @Override
    public ArrayList<Site> getAccessableSites(Site start)
    {
        return board.accessibleSites(start);
    }

    /** @return die Aktuelle Spielphase */
    @Override
    public GamePhase getGamePhase() {
        return board.getGamePhase();
    }

}
