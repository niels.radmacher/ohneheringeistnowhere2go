package nowhere2gopp.io;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import nowhere2gopp.preset.Site;
import nowhere2gopp.preset.SiteSet;

/** Diese Exception wird geworfen im fall, dass ein Spieler aufgegeben hat.*/
class HasSurrenderedException extends Exception{}

/** Diese Klasse definiert den Rahmen auf dem das Spielbrett {@link BoardPanel} und alle
 * anderen Komponenten gezeichnet werden. Ausserdem werden hier die 
 * Mouse events und die Reskalierung des Brettes getriggert */
public class BoardFrame extends JFrame
{
    /** enthält das Komplete Layout des Boards */
    private final JPanel layoutPanel = new JPanel(new BorderLayout(50,50));

    /** dieses Panel enthält alle Zeichenbaren Elemente */
    private final BoardPanel drawBoardPanel;

    /** dieser Viewer ist dafür verantwortlich die für das Zeichnen des Brettes erforderlichen Informationen zu liefern*/
    private final BoardViewer viewer;

    /** bestimmt ob auf eine Site gewartet werden soll */
    private boolean waitForSite = false;

    /** bestimmt ob auf ein SiteSet gewartet werden soll */
    private boolean waitForSiteSet = false;

    /** wahr wenn surrender Button gedrückt wurde */
    private boolean givingUp = false;

    /** zuletzt geglicktes SiteSet */
    private static DrawableLink clickedSiteSet;

    /** zuletzt geklickte Site */
    private static DrawableNode clickedSite;
    
    /** die anzeige im status display*/
    private JLabel label = new JLabel();

    /** das Status display*/
    private JPanel statusDisplay = new JPanel();

    /** Konstruktor diser Klasse
     * @param viewer lifert einen Viewer auf das Spielbrett*/
    public BoardFrame(BoardViewer viewer)
    {
        // setze den Namen des Fensters
        super("nowhere2gopp");

        // setze die standard größe
        setSize(500,500);

        // setze minimale größe
        setMinimumSize(new Dimension(500,500));

        // initalisieren des viewers
        this.viewer = viewer;

        // initalisieren des Panels 
        drawBoardPanel = new BoardPanel( viewer );

        // hier muss einmal BoardColors initalisiert werden.
        BoardColors color = new BoardColors();

        // hinzufügen von einem componentAdapter der die reskalierung des Fensters detektieren kann
        drawBoardPanel.addComponentListener( new ComponentAdapter() {
            public void componentResized(ComponentEvent componentEvent)
            {
                // lösst eine reskalierung aller elemente auf dem Board aus
                updateFrameSize();
            }

        } );

        /* hinzufügen eines mouse listeners um mouse events zu 
         * detektieren dies wird dem panel hinzugefügt damit 
         * der Aufgenommmen Punkt in relativ Koordinaten des 
         * Panels ausgedrückt wird. Ansonsten ergibt sich eine
         * Verschiebung des angecklickten Punktes im Bezug zu 
         * den Elementen auf dem Panel.*/
        drawBoardPanel.addMouseListener( new MouseAdapter(){
            public void mouseClicked(MouseEvent arg0)
            {
                // der geklickte Punkt auf dem canvas
                Point p = arg0.getPoint();
                searchContainingElement(p);
                return;
            }
        } ); 

        // sagt dem Program das es beendet werden soll wenn der JFrame geschlossen wird
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        /* Im folgenden śoll das BorderLayout angewendet werden
         * dazu soll das ganze Panel in 3 zeilen geteilt werden,
         * das untere soll hierbei den surrender Button halten 
         * das Obere die Statusanzeige, sowie das mittlere das 
         * Spielbrett */
        
        // hier wird das Layoutpane zum Frame addiert
        add(layoutPanel);

        /* hier wird das Spielbrett dem Layout hinzugefügt. im
         * Borderlayout wird das element im CENTER immer maximiert
         * wenn skaliert wird dementsprechend wird hier das 
         * Board im Center hinzugefügt. */
        layoutPanel.add(drawBoardPanel, BorderLayout.CENTER);

        /* Hier wird das Panel zur Anzeige des Status hinzugefügt
         * damit der Schriftzug in der MItte platziert wird 
         * wird hier das BoxLayout verwendet. Durch hinzufügen von 
         * Horizontal glue vor und nach dem Schriftzug wird diesr
         * unabhängig von der Skalierung des Fensters in der 
         * Mitte gehalten. */
        BoxLayout box =new BoxLayout(statusDisplay, BoxLayout.LINE_AXIS);
        statusDisplay.setLayout(box);
        statusDisplay.setPreferredSize(new Dimension(0,50));

        // setze den aktuellen status
        setStatus();
        statusDisplay.add(Box.createHorizontalGlue());
        statusDisplay.add(label);
        statusDisplay.add(Box.createHorizontalGlue());

        // füge das status Display überhalb des Brettes hinzu
        layoutPanel.add(statusDisplay, BorderLayout.NORTH);

        /* Hier wird das der Surrender Button dem Frame hinzugefügt
         * um diesen MIttig zu platzieren wird die gleiche
         * Strategie genutzt wie beim Status display */
        JPanel surrenderButton = new JPanel();
        surrenderButton.setPreferredSize(new Dimension(0,50));
        BoxLayout layoutSurrender = new BoxLayout(surrenderButton, BoxLayout.LINE_AXIS);
        surrenderButton.setLayout(layoutSurrender);

        /* hier wird der MouseListener definiert mit dem 
         * der Spieler das Spiel graphisch aufgeben kann*/
        surrenderButton.addMouseListener( new MouseAdapter(){
            public void mouseClicked(MouseEvent e)
            {
                surrender();
            }
        });
        // schriftzug definieren
        JLabel labelSurrender = new JLabel("Surrender");
        labelSurrender.setFont(new Font("Verdana", 1, 20));
        // schriftzug hinzufügen
        surrenderButton.add(Box.createHorizontalGlue());
        surrenderButton.add(labelSurrender);
        surrenderButton.add(Box.createHorizontalGlue());
        
        // füge den Surrender Button unterhalb des Bordes hinzu
        layoutPanel.add(surrenderButton, BorderLayout.SOUTH);
        
    }
    /** wird gerufen wenn aufgegeben wurde, der boolean
     * givingUp wird auf true gesetzt und die Gui notifyt*/
    private synchronized void surrender() 
    {
        givingUp = true;
        notify();
    }


    /** Diese Funktion wird von componentResized aufgerufen
     * und informiert alle Elemente im Panel das sie sich 
     * mit anderer Größe neu zeichnen müssen */
    private void updateFrameSize()
    {
        // informiere das Panel über die neue Frame größe
        drawBoardPanel.updatePanelSize();
    }

    /** Diese Funktion untersucht ob der punkt p in einer dem Frame bekannten Element enthalten ist 
     * @param p der Punkt der untersucht werden soll */
    public synchronized void searchContainingElement(Point p)
    {
        /* da die Links unter den Nodes weiterlaufen muss hier 
         * mit einer Priorität das angeklickte Element gesucht werden
         * dazu wird zuerst in allen nodes gesucht und dann erst in 
         * den links */
        try
        {
            // wirft NotContainedInNodeException wenn nicht enthalten
            clickedSite = drawBoardPanel.getContainingNode(p);
            
            /* überprüfe ob auf eine Site gewartet werden soll
             * wenn ja dann notify das Gui das eine gefunden wurde */
            if(waitForSite == true)
                notify();
            return;
        }
        // der fall das der angeklickte punkt nicht in einem node lag
        catch(NotContainedInNodeException e)
        {
            try
            {
                // wirft NotContainedInLinksException falls der Punkt nicht in einem link liegt
                clickedSiteSet = drawBoardPanel.getContainingLink(p);

                /* überprüfe ob auf ein SiteSet gewartet werden soll
                 * wenn ja dann notify das Gui das eine gefunden wurde */
                if(waitForSiteSet == true)
                    notify();
                return;
            }
            // in diesem Fall wurde kein Element auf dem feld angeklickt
            catch(NotContainedInLinksException exc)
            {
                System.err.println("ERROR not an element of the Board");
            }
        }
    }

    /** überprüft ob der Player aufgegeben hat
     * @return true wenn der Spieler aufgegeben hat
     */
    public boolean isGivingUp() {
        return givingUp;
    }

    /** Gibt das geklickte SiteSet zurück
     * @throws HasSurrenderedException falls der Spieler Aufgegeben hat.
     * @return das geklickte SiteSet
     */
    public SiteSet getClickedSiteSet() throws HasSurrenderedException
    {
        /* überprüft ob der Spieler aufgegben hat, wenn ja dann 
         * benachrichtige das Gui durch das werfen einer HasSurrenderedException */
        if(givingUp)
            throw new HasSurrenderedException();
        
        // markiere das angeklickte element graphisch und aktualisiere das drawPanel
        clickedSiteSet.setFillColor(BoardColors.clickedLinkFill);
        drawBoardPanel.repaint();

        // liefert das angeklickte SiteSet zurück
        return clickedSiteSet.getLink();
    }

    /** Gibt die Gecklickte Site zurück
     * @throws HasSurrenderedException falls der Spieler Aufgegeben hat
     * @return die geklickte Site 
     */
    public Site getClickedSite() throws HasSurrenderedException
    {
        /* überprüft ob der Spieler aufgegben hat, wenn ja dann 
         * benachrichtige das Gui durch das werfen einer HasSurrenderedException */
        if(givingUp)
            throw new HasSurrenderedException();

        // markiere das angeklickte element graphisch und aktualisiere das drawPanel
        clickedSite.setFillColor(BoardColors.clickedNode.get(viewer.getTurn()));
        drawBoardPanel.repaint();
        
        // liefere die angeklichte Site zurück
        return clickedSite.getSite();
    }

    
    /** sagt dem Frame auf welches Element gewartet werden soll
     * @param bool wenn true warte auf Site wenn false warte auf SiteSet*/
    public void waitForSite(boolean bool)
    {
        waitForSite = bool;
        waitForSiteSet = !bool;
    }

    /** sagt dem Frame auf welches Element gewartet werden soll
     * @param bool wenn true warte auf Site wenn false warte auf SiteSet
     * @param startSite die Site von der aus die gültigen sides gezeichnet werden sollen*/
    public void waitForSite(boolean bool, Site startSite)
    {
        try{
            if(bool)
                drawBoardPanel.paintAccessibleLinks(startSite);
        }
        catch(NullPointerException e){}
        waitForSite = bool;
        waitForSiteSet = !bool;
    }
    /** setzt alle Farben des Panels zurück auf eine neutrale Farbe */
    public synchronized void setNeutralColor()
    {
        drawBoardPanel.setNeutralColors();
        notify();
    }
    /** setzt das angezeigte Status Label des Boards */
    public void setStatus()
    {
        // label = new JLabel();
        switch(viewer.getStatus()) 
        {
        case BlueWin:
            label.setText("Blau Gewinnt!");
            break;
        case Draw:
            label.setText("Unentschieden");
            break;
        case Illegal:
            label.setText("Illegaler Zug");
            break;
        case RedWin:
            label.setText("Rot Gewinnt");
            break;
        case Ok:
            switch(viewer.getTurn())
            {
            case Blue:
                label.setText("Blau am Zug");
                break;
            case Red:
                label.setText("Rot am Zug");
                break;
            }
            break;

        }
        label.setFont(new Font("Verdana", 1, 20));
        statusDisplay.repaint();
    }
    public synchronized boolean drawNewTurn()
    {
        drawBoardPanel.setNeutralColors();
        setStatus();
        repaint();
        notify();
        return true;
    }
}
