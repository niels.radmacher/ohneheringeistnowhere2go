package nowhere2gopp.io;

import nowhere2gopp.preset.Viewer;

/** Definiert das Interface Printable welches klassen implementieren
 * welche das Spielbrett ausgeben können */
public interface Printable
{
    /** gibt das spielbrett aus */
    public void printBoard();

    /** setzt den viewer 
     * @param viewer der viewer auf das Brett was ausgegeben werden soll*/
    public void setViewer(Viewer viewer);

    /** liefert zurück ob ein viewer existiert 
     * @return true falls ein viewer != null exisitiert*/
    public boolean hasViewer();
}
