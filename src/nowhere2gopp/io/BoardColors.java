package nowhere2gopp.io;

import java.awt.Color;
import java.util.EnumMap;

import nowhere2gopp.preset.*;

/** Diese Klasse stellt alle nötigen Farben für die graphische 
 * Ausgabe des Brettes bereit.*/
public class BoardColors
{
    /* hier werden oft enummaps genutzt um farben zu definieren,
     * dies wird so gemacht damit man per playercolor die richtige
     * farbe erhalten kann.*/

    /** die standard füll farbe farbe eines nodes*/
    public static final Color nodeFill= new Color(100,100,100);

    /** die standard rand farbe farbe eines nodes*/
    public static final Color nodeBorder= new Color(0,0,0);
    
    /** standard füll farbe der links*/
    public static final Color linkFill= new Color(130,130,130);

    /** standard rand farbe der links*/
    public static final Color linkBorder= new Color(0,0,0);

    /** farbe der Spieler*/
    public static final EnumMap<PlayerColor,Color> players = new EnumMap<>(PlayerColor.class);

    /** füll farbe von selektierten links */
    public static final Color clickedLinkFill = new Color(255,200,0);

    /** rand farbe von selektierten links */
    public static final Color clickedLinkBorder = new Color(100,100,100);
    
    /** hintergrundfarben */
    public static final EnumMap<PlayerColor,Color> background = new EnumMap<>(PlayerColor.class);

    /** enum Map für die farben der angecklickten nodes*/
    public static final EnumMap<PlayerColor,Color> clickedNode = new EnumMap<>(PlayerColor.class);

    /** enum Map für die farben der angecklickte links*/
    public static final EnumMap<PlayerColor,Color> clickedLink = new EnumMap<>(PlayerColor.class);

    /** standard konstruktor, mindestens einmal muss boardcolors initalisiert werden
     * damit die enumaps richtig initalisiert werden.*/
    public BoardColors()
    {
        players.put(PlayerColor.Red, new Color(255,100,100));
        players.put(PlayerColor.Blue, new Color(100,100,255));
        background.put(PlayerColor.Red, new Color(255,150,150));
        background.put(PlayerColor.Blue, new Color(180,180,255));
        clickedNode.put(PlayerColor.Red, new Color(255,113,113));
        clickedNode.put(PlayerColor.Blue, new Color(113,113,255));
    }
}
