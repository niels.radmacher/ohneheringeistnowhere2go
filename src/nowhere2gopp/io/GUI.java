package nowhere2gopp.io;

import nowhere2gopp.board.GamePhase;
import nowhere2gopp.preset.*;



/** Graphische Ausgabe und Eingabe Klasse
 * Diese klasse stellt die graphische Ausgabe des Spieles bereit.
 * Ausserdem wird die graphische Schnitstelle für den menschlichen Spieler bereitgestellt.
 * Um ein Spielbrett anzuzeigen wird hier ein {@link BoardFrame} erstellt und 
 * diesem ein {@link Viewer} auf das Brett übergeben.*/
public class GUI implements Requestable, Printable
{
    /** wird gebraucht um informationen über den Spielzustand abzufragen */
    private BoardViewer viewer;

    /** der Frame in der die Abfrage der geklickten elemnte passiert und alle draw-events handelt*/
    private final BoardFrame frame;

    /** Konstruktor
     * @param viewer ein viewer auf das Brett,
     * welches graphisch dargestellt werden soll */
    public GUI(BoardViewer viewer)
    {
        this.viewer = viewer;
        this.frame = new BoardFrame(viewer);
        this.frame.setVisible(true);
    }

    /** Diese Funktion erlaubt es den viewer der GUI zu ändern,
     * dies wird gebraucht um den Netzwerkspieler zu realisieren 
     * @param viewer der neue viewer der GUI*/
    public void setViewer(Viewer viewer)
    {
        this.viewer = (BoardViewer)viewer;
    }

    /** Diese Funktion prüft ob diese GUI momentan einen definierten
     * Viewer hat.
     * @return wahr wenn viewer != null */
    public boolean hasViewer()
    {
        return !(viewer == null);
    }

    /** Diese Funktion liefert einen Spielzug zurück, von dem noch nicht
     * überprüft wurde ob dieser legal ist 
     * @return graphisch eingelesener Zug*/
    @Override
    public Move request() throws Exception
    {
        printBoard();
        /* Dies ist die Erste Spielphase. In dieser soll jeder 
         * Spieler pro Zug genau zwei links entfernen. 
         * Die Spieler sind zu diesem Zeitpunkt noch nicht im 
         * Spiel und werden deshalb vernachlässigt*/
        try
        {
            if(viewer.getGamePhase() == GamePhase.LinkLink)
            {
                System.out.println("Gefordereter Zug: LinkLink");

                // initalisiere die zwei nötigen zug variablen
                SiteSet firstSet=null, secondSet=null;

                synchronized(frame) 
                {
                    // lese das erste set
                    frame.waitForSite(false);

                    while(firstSet == null)
                    {
                        frame.wait();
                        firstSet = frame.getClickedSiteSet();
                    }
                    
                    // lese das zweite set
                    while(secondSet == null)
                    { 
                        frame.wait();
                        secondSet = frame.getClickedSiteSet();
                    }
                }

                Move returnMove = new Move(firstSet,secondSet);

                // return returnMove;
                System.out.println(returnMove.toString());
                return returnMove;
            }
            else
            {
                System.out.println("Gefordereter Zug: AgentLink");

                /* die nötigen variablen werden deklariert
                 * beachte oldPlayerPosition wird auf die letzte poistion
                 * von dem Spieler gesetzt; Hier wird erwartet das null 
                 * zurückgeliefert wird wenn der Spieler noch nicht gesetzt 
                 * wurde*/
                Site oldPlayerPosition = null;
                Site newPlayerPosition = null;
                SiteSet removeLink = null;

                try{
                    oldPlayerPosition = viewer.getAgent(viewer.getTurn());
                }
                catch(NullPointerException e){}

                synchronized(frame)
                {
                    // in allen fällen muss zuerst auf eine Site gewartet werden
                    frame.waitForSite(true);

                    /* hier muss abgefangen werden das der Spieler schon 
                     * auf dem brett steht, wenn nicht soll hier die Poistion
                     * eingelesen werden*/
                    if(oldPlayerPosition == null)
                    {
                        // wenn nicht wird hier die Position eingelesen
                        while(oldPlayerPosition == null){
                            frame.wait();
                            oldPlayerPosition = frame.getClickedSite();
                        }
                    }

                    // sorgt dafür das die möglichen nodes eingemahlt werden
                    frame.waitForSite(true, oldPlayerPosition);

                    // hier wird die neue position eingelesn
                    while(newPlayerPosition == null){
                        frame.wait();
                        newPlayerPosition = frame.getClickedSite();
                    }

                    System.out.print("hier");

                    /* hier wird der zu entfernde link eingelesen
                     * zuerst muss dafür der observer auf siteset
                     * gestellt werden. danach kann der thread 
                     * benachrichtigt werden */
                    frame.waitForSite(false);

                    while(removeLink == null) {
                        frame.wait();
                        removeLink = frame.getClickedSiteSet();
                    }
                }

                // Der Zug wird initialisiert und zurückgeliefert
                Move returnMove = new Move(new SiteTuple(oldPlayerPosition,newPlayerPosition), removeLink);
                System.out.println(returnMove);
                return returnMove;
            }
        }
        /* Sollte der surrender Button genutzt werden wird 
         * eine HasSurrenderedException geworfen. Dies 
         * garantiert das die Aktuelle Zug aufnahme egal in
         * welchen Stadium sie gerade ist beendet werden kann.
         * Um den Hauptprogram zu sagen das das Spiel 
         * aufgegeben wurde wird dann ein Move mit Movetype
         * Surrender zurückgeliefert.                   */
        catch(HasSurrenderedException e)
        {
            return new Move(MoveType.Surrender);
        }
    }

    /** diese funktion sorgt für ein neu ausgeben des Spielbrettes*/
    @Override
    public void printBoard() {

        /* um ein Board zu mahlen werden immer die folgenden
         * Schritte ausgeführt: als erstes wird die Farbe 
         * aller Elemente auf ihre normale Farbe gesetzt 
         * danach wird die status anzeige aktualisiert und
         * als letztes ein repaint des kompletten frames 
         * getriggert.*/

        try
        {
            boolean redraw = false;
            redraw = frame.drawNewTurn();
            while(!redraw)
            {
                frame.wait();
            }
        
        }
        catch(Exception e){}
    }
}
