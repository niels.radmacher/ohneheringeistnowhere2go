package nowhere2gopp.io;

import java.awt.Color;
import java.awt.Shape;

/** dieses interface definiert was ein Zeichenbares Element
 * des Spielbrettes an funktionen haben muss wird genutzt
 * um verschiedene Funktionen zu vereinfachen.*/
public interface DrawableBoardElement extends Shape
{
    /** gibt die füll farbe des Elements
     * @return die Farbe des Elements */
    public Color getFillColor();

    /** setzt die füll farbe des Elements 
     * @param newFillColor die neue Farbe */
    public void setFillColor(Color newFillColor);

    /** gibt die rand Farbe des Elements
     * @return die Farbe des Randes*/
    public Color getBorderColor();

    /** setzt die rand Farbe des Elements
     * @param newBorderColor die rand Farbe des Elements*/
    public void setBorderColor(Color newBorderColor);

    /** passt die Elementgröße an */
    public void adaptElementSize();
}
