package nowhere2gopp.io;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.geom.*;

import nowhere2gopp.preset.Site;
import nowhere2gopp.preset.SiteSet;

/** Ein Zeichebares SitePair, definiert wie dieses gezeichnet wird 
 * erweitert Polygon damit Elemente von diesem Typ direkt auf BoardPanel 
 * gezeichnet werden können*/
public class DrawableLink extends Polygon implements DrawableBoardElement
{
    /** enthält das SiteSet*/
    private final SiteSet link;

    /** enthält die Füll Farbe*/
    private Color fillColor = BoardColors.nodeFill;

    private Color borderColor = BoardColors.nodeBorder;

    /** bietet informationen zum Board und Panel (skalierung, Koordinaten)*/
    private final BoardInfo info;

    /** p0 ist der Anfangspunkt und p1 der Endpunkt der linie in unskalierten Einheiten*/
    private final Point2D.Double p0,p1;

    // enthält die Eckpunkte des Polygons 
    
    /** eckpunkt 1*/
    private final Point2D.Double edgeOfSiteRect0 = new Point2D.Double();
    /** eckpunkt 2*/
    private final Point2D.Double edgeOfSiteRect1 = new Point2D.Double(); 
    /** eckpunkt 3*/
    private final Point2D.Double edgeOfSiteRect2 = new Point2D.Double(); 
    /** eckpunkt 4*/
    private final Point2D.Double edgeOfSiteRect3 = new Point2D.Double(); 

    /** Konstruktor 
     * @param link der link für den ein Zeichenbares Element erstellt werden soll
     * @param info liefert koordinaten in relation zum Panel auf dem gemahlt wird. */
    public DrawableLink(SiteSet link, BoardInfo info) 
    {

        // initalisieren von link und info
        this.link = link;
        this.info = info;

        // erhalte die Sites am linken und rechten ende vom link
        Site first = link.getFirst();
        Site second = link.getSecond();

	// erhalte die nötigen koordinaten zum zeichnen der Links von info
        p0 = info.getCoord(first.getColumn(),first.getRow());
        p1 = info.getCoord(second.getColumn(),second.getRow());

	// passe die Element größe an
        adaptElementSize();
    }


    /**
     * @return der link
     */
    public SiteSet getLink() {
        return link;
    }


    /**
     * @param color die Füll Farbe
     */
    @Override
    public void setFillColor(Color color) {
        this.fillColor = color;
    }

    /** @return die RandFarbe des Links*/
    @Override
    public Color getBorderColor() {
        return borderColor;
    }

    /** @param newBorderColor die Neue Rand farbe des Links*/
    @Override
    public void setBorderColor(Color newBorderColor) {
        borderColor = newBorderColor;
    }

    @Override
    /** setze die Eckpunkt des Polygons richtig zur aktuellen skalierung*/
    public void adaptElementSize() 
    {
        double scale = info.getScale();

        /*         +' 
         *       +  ' 
         *     +    ' Y1        
         *   +α)    '    
         * +--------'
         *     X1		    
         * Gezeigt: die punkte markiren eine Linie zu der ein Polygon gefunden werden soll */ 
        double X1 = p0.getX() - p1.getX();
        double Y1 = p0.getY() - p1.getY();

        /* um konsitente linien breiten zu erhalten kann mit hilfe von trigometire 
         * gezeigt werden das ein Eckpunkt immer bei x = w/2*sin(α) und y = w/2*cos(α) 
         * liegen muss mit α dem winkel zwischen der hypotinuse und X1 oben 
         * damit erhält man α zu atan(Y1/X1). dieser Ausdruck lässt sich dann zu den 
         * untenstehenden Ausdrücken umformen. w ist dabei die gewünschte Breite der
         * Linie*/ 
        double x = scale/10. * (Y1/X1)/Math.sqrt(Math.pow(Y1/X1,2) + 1);
        double y = scale/10. * 1/Math.sqrt(Math.pow(Y1/X1,2) + 1);

        // setze die Eckpunkte des Polygons 
        edgeOfSiteRect0.setLocation((p0.getX())*scale - x - info.getXoff(), (p0.getY())*scale + y);
        edgeOfSiteRect1.setLocation((p1.getX())*scale - x - info.getXoff(), (p1.getY())*scale + y);
        edgeOfSiteRect2.setLocation((p1.getX())*scale + x - info.getXoff(), (p1.getY())*scale - y);
        edgeOfSiteRect3.setLocation((p0.getX())*scale + x - info.getXoff(), (p0.getY())*scale - y);

        // das alte polgon reseteen
        reset();

        // zum polygon nacheinander in richtiger Reihenfolge die Eckpunkte hinzufügen
        addPoint((int) edgeOfSiteRect0.getX(), (int) edgeOfSiteRect0.getY());
        addPoint((int) edgeOfSiteRect1.getX(), (int) edgeOfSiteRect1.getY());
        addPoint((int) edgeOfSiteRect2.getX(), (int) edgeOfSiteRect2.getY());
        addPoint((int) edgeOfSiteRect3.getX(), (int) edgeOfSiteRect3.getY());

    }

    /** @return die akutelle FüllFarbe des Links */
    @Override
    public Color getFillColor() {
        return fillColor;
    }
}
