package nowhere2gopp.io;

import java.util.Scanner;
import java.io.FilterInputStream;
import java.io.IOException;

import nowhere2gopp.preset.*;

/** Diese Klasse stellt die Funktionalität des
 * Einlesens von Zügen von der Kommandozeile bereit*/
public class TextInput implements Requestable
{
    private Move playerMove;

    /** hier wird ein zug von der KomandoZeile eingelesen 
     * @return den eingelesenn Zug*/
    public Move readMove()
    {
        /* öffne einen scanner so das der Input nicht geschlossen
         * werden kann wenn der scanner geschlossen wird. Ansonsten
         * müsste ein scanner durchgehend geöffnet bleiben. */
        Scanner sc = new Scanner(new FilterInputStream(System.in){
            @Override
            public void close() throws IOException{}
        });

        // einlesen von der Komandozeile
        String nextMoveString = sc.next();
        
        // fangen aller möglichen Exceptions von move.parse()
        try
        {
            playerMove = Move.parse(nextMoveString);
        }
        catch(SiteFormatException e)
        {
            System.out.println(e.getMessage());
        }
        catch(MoveFormatException e)
        {
            System.out.println(e.getMessage());
        }
        catch(SitePairFormatException e)
        {
            System.err.println(e.getMessage());
        }
        sc.close();
        return playerMove;
    }

    @Override
    public Move request() throws Exception 
    {
        // sicherstellen das vorige eingaben entfernt werden
        playerMove = null;

        // zug einlesen bis ein erfolgreicher move geparst wurde
        while(playerMove == null)
            playerMove = readMove();

        // rückgabe des wertes
        return playerMove;
    }
}

