package nowhere2gopp.io;

import javax.swing.*;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import nowhere2gopp.preset.PlayerColor;
import nowhere2gopp.preset.Site;
import nowhere2gopp.preset.SiteSet;

/** definiert eine Exception für suchen in allen Nodes*/
class NotContainedInNodeException extends Exception
{
    public NotContainedInNodeException() {
        super();
    }
    public NotContainedInNodeException(String msg)
    {
        super(msg);
    }
}

/** definiert eine Exception für suchen in allen links*/
class NotContainedInLinksException extends Exception
{
    public NotContainedInLinksException() {
        super();
    }
    public NotContainedInLinksException(String msg)
    {
        super(msg);
    }
}

/** Dieses Panel zeichnet alle Elemente zur Darstellung des Spielbrettes.
 * Um links darzustellen werden dazu {@link DrawableLink} sowie bei Nodes {@link DrawableNode} genutzt.
 * Das Board ist für die Farben aller Elemente verantwortlich und kann das Ausgegbene Brett 
 * im Falle einer Fensergrößen veränderung richtig der neuen Spielfeldgröße anpassen.*/
public class BoardPanel extends JPanel
{
    /** mithilfe von viewer können die fürs zeichnen des Brettes nötigen informationen abgefragt werden*/
    private  BoardViewer viewer;

    /** info übernimmt Board spezifische Rechenarbeiten und berechnet den skalierungsfaktor */
    private  BoardInfo info;

    /** in nodes werden die einzelnen Zeichenbaren Nodes des Brettes gespeichert */
    private  final ArrayList<DrawableNode> nodes = new ArrayList<DrawableNode>();

    /** in links werden die Zeichenbaren Links gespeichert */
    private  ArrayList<DrawableLink> links = new ArrayList<DrawableLink>();

    @Override
    public Dimension getPreferredSize()
    {
        return new Dimension(500,500);
    }

    /** standard Constructor 
     * @param viewer gibt dem BoardPanel eine viewer klasse die nötig ist um informationen
     * zum Zustand des Spielbrettes abzufragen.*/
    public BoardPanel(BoardViewer viewer)
    {
        // initalisieren des Viewers
        this.viewer = viewer;

        // erhalte die Größe des Brettes
        int n = viewer.getSize();

        // erstelle eine neue Boardinfo klasse
        info = new BoardInfo(n, (new Dimension(500,500)));

        // initalisieren der Nodes mit der Richtigen Zeile und Spalte
        for (int i=0; i<n; i++)
        {
            if(i<=n/2)
                for (int j=0; j < n-Math.abs(n/2-i); j++) 
                    nodes.add(new DrawableNode(new Site(i,j), info, info.getCoord(i,j)));
            else
                for (int j=Math.abs(n/2-i); j <n; j++) 
                    nodes.add(new DrawableNode(new Site(i,j), info, info.getCoord(i,j)));
        }

        // kriege die aktuellen links zwischen nodes und initalisiere sie
        Collection<SiteSet> currentLinks =  viewer.getLinks();
        for(SiteSet link : currentLinks)  
            links.add(new DrawableLink(link, info));
    }

    /** Vergleicht die gespeicherten links mit den neuen 
     * die von viewer übergeben werden und entfernt alle
     * nicht benötigten links aus dieser ArrayList */
    public void updateExistingLinks()
    {
        // die aktualisierte Fassung der Links
        Collection<SiteSet> currentLinks = viewer.getLinks();

        // erstelle einen Iterator über die derzeitigen DrawableLinks
        Iterator<DrawableLink> iter = links.iterator();
        
        // solange es noch Elemente in dieser Liste gibt...
        while(iter.hasNext())
        {
            // erhalte den DrawableLink in iterationsreihenfolge
            DrawableLink currentLink = iter.next();
            
            // falls nicht enthalten in den aktuellen links entfernen.
            if( !( currentLinks.contains(currentLink.getLink()) ) )
                iter.remove();
        }
    }

    /** diese funktion lösst eine reskalierung aller elemnente auf dem Brett aus 
     * und sorgt für eine neu Ausgabe aller Elemente in dieser Angepassten größe*/
    public void updatePanelSize()
    {
        Rectangle boundingBox = this.getBounds();
        // update skalierung
        info.updateScale(boundingBox);
        // update jeden Link
        for(DrawableLink link : links)
            link.adaptElementSize();
        // update jeden Node
        for(DrawableNode node : nodes)
            node.adaptElementSize();
        // neu mahlen des Panels
        this.repaint();
    }

    /** mahlt ein Element auf das Brett 
     * @param elementToDraw das Element welches gemahlt werden soll
     * @param g2d dies Graphic auf der gemahlt werden soll*/
    private void drawBoardElement(DrawableBoardElement elementToDraw, Graphics2D g2d)
    {
        // fülle die Elemente mit einer Farbe
        g2d.setColor(elementToDraw.getFillColor());
        g2d.fill(elementToDraw);

        // zeichne einen Rand um die Elemente
        g2d.setColor(elementToDraw.getBorderColor());
        g2d.draw(elementToDraw);
    }

    /** der überschriebene geerbte paintComponent funktion wird immer 
     * ausgelösst wenn dieses Panel zu einem Frame geaddet wird
     * @param g die graphik auf der gemahlt wird */
    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        // setze hintergrundfarbe jeh nach spieler zug
        this.setBackground(BoardColors.background.get(viewer.getTurn()));

        // aktualisiere die liste der DrawableNodes
        updateExistingLinks();

        // dieser Cast ist immer möglich! deswegen keine überpfrüfung
        Graphics2D g2d = (Graphics2D) g;

        // mahle alle links
        for(DrawableBoardElement link : links)
            drawBoardElement(link, g2d);

        // setze die Farbe der player Nodes
        for(PlayerColor playerColor : PlayerColor.values())
        {
            try
            {
                Site newSite = viewer.getAgent(playerColor);
                for(DrawableNode node : nodes)
                    if(node.getSite().equals(newSite))
                        node.setFillColor(BoardColors.players.get(playerColor));
            }
            // falls die Spieler noch nicht gesetzt wurden muss hier eine NullPointerException gefangen werden
            catch(NullPointerException e){}
        }

        // mahle alle Nodes
        for(DrawableNode node : nodes)
        {
            drawBoardElement(node, g2d);
            g2d.drawString(node.getSite().toString(), (int) (node.getX()), (int)(node.getY()));
        }

    }

    /** überprüfe ob Punkt in den Nodes enthalten ist
     * @param p Punkt der Übeprüft werden soll
     * @throws NotContainedInNodeException wird geworfen wennn der punkt nicht enthalten ist
     * @return der Node welcher den punkt p enthält*/
    public DrawableNode getContainingNode(Point p) throws NotContainedInNodeException
    {
        for(DrawableNode node : nodes)
            if(node.contains(p))
                return node;

        throw new NotContainedInNodeException();
    }

    /** überprüfe ob punkt in einem link liegt 
     * @param p der Punkt der überprüft werden soll
     * @throws NotContainedInLinksException falls der Punkt in keinem bekanten link liegt
     * @return der Link der den punkt p enthält*/
    public DrawableLink getContainingLink(Point p) throws NotContainedInLinksException
    {
        for(DrawableLink link : links)
            if(link.contains(p))
                return link;

        throw new NotContainedInLinksException();
    }

    /** setzt die farben auf dem feld zu neutralen zurück
     * (von selektiert, player, usw.) */
    public void setNeutralColors()
    {
        for(DrawableNode node : nodes)
            node.setFillColor(BoardColors.nodeFill);
        for(DrawableLink link : links)
            link.setFillColor(BoardColors.linkFill);
    }

    /** zeichnet die möglichen anklickbaren nodes in einer farbe an
     * @param startSite parameter der bestimmt von wo aus überprüft werden soll*/
    public void paintAccessibleLinks(Site startSite)
    {

        ArrayList<Site> possibleMoves = viewer.getAccessableSites(startSite);
        for(DrawableNode availableSite : nodes)
        {
            if(!(possibleMoves.contains(availableSite.getSite())))
                availableSite.setFillColor(BoardColors.background.get(viewer.getTurn()));
        }
        this.repaint();
    }

}
