package nowhere2gopp.io;

import java.awt.geom.Point2D;
import java.awt.Dimension;
import java.awt.Rectangle;

/** Diese Klasse liefert Informationen über das Spielbrett, 
 * wie zum Beispiel die Höhe und die Breite */
public class BoardInfo
{
    /** die Größe des Brettes */
    private final int n;

    /** der Skalierungsfaktor der nötig ist um das Brett auf den Frame anzupassen*/
    private double scale;

    /** die Größe des Brettes in nicht skallierten Einheiten (der Abstand zwischen zwei nodes wird 1 gesetzt)*/
    private double boardWidth,boardHeight;

    /** variable zum verschieben des Brettes in die Mitte der Graphischen Anzeigefläche*/
    private int xOffset;

    /** Konstruktor der Klasse 
     * @param n größe des brettes (Anzahl der Spalten/Zeilen)
     * @param frameDimension die Größe des Frames*/
    public BoardInfo(int n, Dimension frameDimension)
    {
        // setze die Board größe
        this.n = n;

        /* Berechne die Breite und die Höhe des Feldes
         * das Brett hat die Maximale Breite von n-1
         * nodes */
        boardWidth = n-1;

        // die Höhe kann durch trigonometrie berechnet werden zu: (n-1)*sin(60)
        boardHeight = (Math.sqrt(3.0)/2.0 * (n-1));

        // initalisierung des skalierungsfaktors
        updateScale(new Rectangle(frameDimension));

    }

    /** Berechnet die Kartesische Koordinate aus der gegebenen Reihe und Spalte 
     * @param row reihe des Objekts
     * @param col spalte des Objekts
     * @return Ein Object des Typs Coordinate welche die Kartesische Koordinate enthält*/
    public Point2D.Double getCoord(int row, int col)
    {
        double x = 0,y = 0;

        // x = cos(60) |row - n/2| + col + offset; n ist immer ungerade -> wird abgerundet 
        x =  (-0.5 * col + row) + (n-1)/4.;
        // y = sin(60°)*row
        y =  boardHeight - Math.sqrt(3)/2 * col + 2/4.;

        // rückgabe wert 
        Point2D.Double erg = new Point2D.Double(x,y);
        return erg;
    }

    /** Liefert die höhe des Spielfelds
     * @return höhe des Feldes in skalierten Einheiten */
    public double getBoardHeight()
    {
        return scale*boardHeight;
    }

    /** Liefert die breite des Spielfelds
     * @return breite des Feldes in skalierten Einheiten */
    public double getBoardWidth()
    {
        return scale*boardWidth;
    }

    /** berechnet eine neuen skalier wert um das Spielfeld passend zum frame darzustellen
     * @param panelSize die höhe und die Breite des Panels auf dem das Brett gemahlt wird*/
    public void updateScale(Rectangle panelSize)
    {

        /* hier werden die zwei skalierungen nach x und y 
         * richtung berechnet. Davon wird hinterher der Faktor
         * genommen welcher kleiner ist um eine korrekte anzeige
         * des Brettes zu ermöglichen. zur BoardBreite und Weite 
         * wird hier immer 3/2 hinzugerechnet um den Umfang der 
         * Nodes mit einzubeziehen. */
        double scale1 = (panelSize.getWidth())/(boardWidth+3./2.);
        double scale2 = (panelSize.getHeight())/(boardHeight+3./2.);
        
        // die breite des Frames
        int frameWidth = (int) panelSize.getWidth();

        // selektiere den kleineren wert
        scale = scale1 > scale2 ? scale2 : scale1;
        // setze den xoffset der nötig ist um das brett mittig darzustellen
        setXoff(frameWidth);
        return;
    }

    /** funktion zum setzen des x offsets, wird nur aus Board
     * info aufgerufen und ist deshalb privat.
     * @param panelWidth die Weite des Feldes*/
    private void setXoff(int panelWidth)
    {
        // berechne das neue xOffset
        xOffset = (int) (boardWidth*scale - panelWidth)/2;

        // achte darauf das xoffset größer als null ist
        xOffset = xOffset > 0 ? 0 : xOffset;
    }
    
    /** @return den xoffset */
    public int getXoff()
    {
        return xOffset;
    }

    /** liefert den skalierungsfaktor zurück der nötig ist um das brett
     * passend zur frame größe darzustellen
     * @return scaling factor*/
    public double getScale()
    {
        return scale;
    }

}
