package nowhere2gopp.player;
import nowhere2gopp.preset.*;
import nowhere2gopp.board.GamePhase;
import java.util.*;
import nowhere2gopp.io.Printable;

/**
 *AI Spieler der mit einer einfachen Gewichtung Züge auswählt 
 */
public class AdvancedAIPlayer extends BoardPlayer{ 

	/**
	 *Erstellt neuen AdvancedAIPlayer ohne grafische Ausgabe.
	 */
	public AdvancedAIPlayer(){
		super();
		this.type = PlayerType.AdvancedAI;
	}

	/**
	 *Erstellt neuen AdvancedAIPlayer mit grafische Ausgabe.
	 *@param output Printable Element zur grafischen Ausgabe.
	 */ 
	public AdvancedAIPlayer(Printable output){
		super(output);
		outputUsingMyBoard = true;
		this.type = PlayerType.AdvancedAI;
	}

	/**
	 *Gibt neuen Move zurück und setzt diesen auf eigenem Board
	 *@return neu berechneter Zug
	 */ 
	public synchronized Move request() throws Exception{ 
		//nächster Spielzug
		Move nextMove = null;
		ArrayList<SiteSet> links = localBoard.getLinks();
		int linkNumber = links.size();
		Site startSite = null;
		
		switch(localBoard.getGamePhase()){

			//erste Spielphase
			case LinkLink:
				boolean different = false;
				int ran1 = 0, ran2 = 0;

				//Zwei zufällig gewählte links, die unterschiedlich sind, wählen
				while(!different){
					ran1 = (int) (Math.random() * (linkNumber-1));
					ran2 = (int) (Math.random() * (linkNumber-1));
					if(ran1 != ran2)
						different = true;
				}

				//die beiden links zwischenspeichern zur übersichtlichkeit
				SiteSet link1 = links.get(ran1);
				SiteSet link2 = links.get(ran2);

				nextMove = new Move(link1, link2);
				break;
			

			//zweite Spielphase
			case SetAgentLink:
				boolean best = false;
				int size = localBoard.getSize();
				Site newSite;
				startSite = new Site(0,0);
				//startSite festlegen indem man Site auswählt, die auf der größten Zusammenhangkomponente liegt
				for(int i=0; i < localBoard.getSize() ; i++){
					for(int j = 0 ;j < localBoard.getSize(); j++)
						if(localBoard.isOnBoard(i,j) && localBoard.accessibleSites(new Site(i,j)).size() > localBoard.accessibleSites(startSite).size())
							startSite = new Site(i,j);
				}

			case AgentLink:
				//Dritte Spielphase

				//Der zu entfernende Link
				SiteSet removeLink = null;
				//Das SiteTuple für den neuen Move
				SiteTuple agentSite = null;
				//Die neue Site auf die man springen will
				newSite = null;
				//Arraylist und Anzahl der benachbarten Links des Gegner
				ArrayList<SiteSet> neighLinks = null;
				int numberOfLinks = 0;

				//Falls er nicht mehr in Phase 2 ist, kann er auf die Gegnerposition zugreifen
				if(localBoard.getGamePhase() == GamePhase.AgentLink){
					neighLinks = localBoard.getNeighborLinks(localBoard.getAgent(opponentColor));
					numberOfLinks = neighLinks.size();
					startSite = localBoard.getAgent();
				}
				
				//Falls der Gegner keine Nachbarlinks mehr hat, such neue Site und lösch beliebigen Link
				if(numberOfLinks == 0 && localBoard.getGamePhase() == GamePhase.AgentLink){
					newSite = searchSite(startSite);
					ran1 = (int) (Math.random() * (linkNumber-1));
					removeLink = links.get(ran1);
					agentSite = new SiteTuple(startSite, newSite);
				}

				//Falls der Gegner einen Nachbarlink hat such neue Site und lösch seinen Link
				if(numberOfLinks == 1){
					removeLink = neighLinks.get(0);
					newSite = searchSite(startSite);
					agentSite = new SiteTuple(startSite, newSite);
				}

				//Falls der Gegner zwei Nachbarlinks hat schau ob man sich auf eine der beiden Sites setzen kann
				//und schneid den anderen Link ab  
				if(numberOfLinks == 2){
					ArrayList<Site> sites = localBoard.getNeighborSites(localBoard.getAgent(opponentColor));
					if(localBoard.isPath(localBoard.getAgent(),sites.get(0))){
						newSite = sites.get(0);
						removeLink = new SiteSet(sites.get(1), localBoard.getAgent(opponentColor));
					}
					if(localBoard.isPath(localBoard.getAgent(),sites.get(1))){
						newSite = sites.get(1);
						removeLink = new SiteSet(sites.get(0), localBoard.getAgent(opponentColor));
					}
					else{
						int ran = (int) (Math.random() * (numberOfLinks-1));
						removeLink = neighLinks.get(ran);
						newSite = searchSite(startSite);
					}
					agentSite = new SiteTuple(startSite, newSite);
				}
				
				
				//Falls der Gegner mehr als zwei Möglichkeiten hat zu fliehen wird ein
				//benachbarter Link entfernt
				if(numberOfLinks != 1 && numberOfLinks != 2){
					if(localBoard.getGamePhase() == GamePhase.AgentLink){
						int ran = (int) (Math.random() * (neighLinks.size() - 1));
						removeLink = neighLinks.get(ran);
						newSite = searchSite(startSite);
						agentSite = new SiteTuple(startSite, newSite);
					}
					else{		
						//nur die Links die nicht neben einem selbst liegen 
						ArrayList<SiteSet> goodLinks = links;
						goodLinks.removeAll(localBoard.getNeighborLinks(startSite));
						if(goodLinks.size() != 0){
							ran1 = (int) (Math.random() * (goodLinks.size()-1));
							removeLink = goodLinks.get(ran1);
						}
						else{
							ran1 = (int) (Math.random() * (linkNumber-1));
							removeLink = links.get(ran1);
						}

						newSite = searchSite(startSite);
						agentSite = new SiteTuple(startSite, newSite);
					}
				}

				nextMove = new Move(agentSite, removeLink);
				break;
			}

		//Move auf eigenem Board setzen und Move zurückgeben
		localBoard.make(nextMove);
		playerStatus = PlayerStatus.Requested;
		return nextMove;
		
	}

	/**
	 *Sucht für den Agenten auf Position der uebergebenen Site neue Site mit möglichst vielen Links.
	 *@param startSite Site des Agenten
	 *@return erreichbare Site mit möglichst vielen NachbarLinks
	 */
	public Site searchSite(Site startSite){
		ArrayList<Site> sites = localBoard.accessibleSites(startSite);
		int siteNumber = sites.size();
		Site newSite = sites.get(0);

		//Suchen der Site mit den meisten entakten Links 
		for(int i=0;i < siteNumber;i++){
			if(localBoard.getNeighborLinks(sites.get(i)).size() > localBoard.getNeighborLinks(newSite).size())
				newSite = sites.get(i);
		}
		return newSite;
	}

}