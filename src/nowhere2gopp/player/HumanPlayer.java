package nowhere2gopp.player;
import nowhere2gopp.io.GUI;
import nowhere2gopp.io.Printable;
import nowhere2gopp.io.TextInput;
import nowhere2gopp.preset.*;
import nowhere2gopp.board.*;

/**Klasse für einen menschlichen Spieler für nowhere2go.
*Spielzüge werden über das Interface {@link nowhere2gopp.preset.Requestable} abgefragt.
*Zudem kann der Spielstand über das Interface {@link nowhere2gopp.io.Printable} 
*angezeigt werden.
*/
public class HumanPlayer extends BoardPlayer{

	/**Konsstruiert einen Spieler Objekt, dass von Einem Menschen bedient 
	*werden kann.
	*Diese kann von {@code input} einen {@code Move} durch die Methode 
	*{@link nowhere2gopp.preset.Requestable#request()} abfragen.
	*@param input
	*		Eingabe der Züge
	*@param output
	*		grafische Ausgabe des Spielstands
	*/
	public HumanPlayer(Requestable input, Printable output){
		super(output);
		this.input = input;
		this.output = output;
		outputUsingMyBoard = true;
		inputUsingMyBoard = true;
		if(input instanceof TextInput)
			inputUsingMyBoard = false;
		this.type = PlayerType.Human;
	}

	/**Erzeugt einen spielbaren Spieler. Dieser verfügt über keinen eigene grafische Augabe
	*@param input 
	* 		Eingabe der Züge
	*/
	public HumanPlayer(Requestable input){
		super();
		this.input = input;
		this.type = PlayerType.Human;
	}

	/**Fragt einen neuen Zug vom input nach. Hier werden nur legale Züge verabeitet. Bei einem 
	*nicht legalen Move wird einen Fehlermeldung erzeugt und der Spiler erneut um einen Zug 
	*gebeeten. Da die Methode {@link nowhere2gopp.board.Board#isLegalMove(Move, PlayerColor)}
	* einen Exception wirft wenn einen Zug illegal ist wird die gefangen und in die Fehlermeldung
	* umgewandelt.
	*@return neuer Zug
	*@throws Exception 
	*		wenn der Reihfolge der Methodenaufrufe nicht eingehalten wird
	*/
	public Move request() throws Exception{
		if(!isInitialized)
			throw new Exception("Player muss mit init() initialisiert werden");

		if(playerStatus != PlayerStatus.Updated)
			throw new Exception("Die Reihenfolge beim Methodenaufruf wurde nicht eingehalten." +
				" in request()");

		/**Flag zu zur Kontrolle der Übergabe eines Zuges von der GUI*/
		boolean correctMove = false;

		Move nextMove = null;
		while(!correctMove){
			try{

				nextMove = input.request();

				switch (localBoard.getGamePhase()) {
					case LinkLink:
						if(nextMove.getType() == MoveType.AgentLink)
							throw new Exception("In Phase 1 nur LinkLink, Surrender oder End");
						else{
							/*Nur Legane Moves werden auch ausgeführt*/
							if(localBoard.isLegalMove(nextMove, color)){
								localBoard.make(nextMove);
								correctMove = true;
							}
						}
						break;
					case AgentLink:
					case SetAgentLink:
						if(nextMove.getType() == MoveType.LinkLink)
							throw new Exception("In Phase 2 und 3 nur AgentLink, Surrender oder End");
						else{
							/*Nur Legane Moves werden auch ausgeführt*/
							if(localBoard.isLegalMove(nextMove, color)){
								localBoard.make(nextMove);
								correctMove = true;
							}
						}
						break;
					default:
							throw new Exception("Ungültige Spielphase");	
				}
			}catch (IllegalStateException e) {
				System.err.println("Ungültiger Zug übergeben " + e.getMessage());
			}
			catch (Exception e) {
				System.err.println("Fehler beim Abfragen eines Moves von GUI " + e.getMessage());
			}
		}

		if(output != null){
			output.printBoard();
		}
		playerStatus = PlayerStatus.Requested;
		return nextMove;
	}
}



