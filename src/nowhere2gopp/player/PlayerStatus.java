package nowhere2gopp.player;

/**Beeinhalted die drei möglichen Zustände, die ein Spieler haben kann,
*damit die Reihenfolge der Methodenaufrufe eingehalten wird.*/
public enum PlayerStatus{
	Requested,
	Confirmed,
	Updated
}