package nowhere2gopp.player;
import nowhere2gopp.preset.*;
import nowhere2gopp.board.Board;
import nowhere2gopp.board.GamePhase;
import javax.swing.*;
import java.util.*;

/**
 *Thread der Gewichtung möglicher Züge ausrechnet
 */
class WeightThread extends Thread{
	
	/**Kopie des übergebenen Boards*/
	private Board copyBoard;
	/**Übergebener Zeiger auf Array in dem an Stelle {@link WeightThread#index} die berechnete weight eingespeichert wird*/
	int weight[];
	/**Ist die momentan in {@link EnhancedAIPlayer} beste Weight*/
	private int currentweight;
	/**Ist der Index der dem Thread von {@link EnhancedAIPlayer} zugeordnet wurde */
	private int index;

	/**
	 *Erstellt neuen Thread und weist ihm ein Board zu.
	 *@param board Neues Board des Threads  
	 *@param weight Momentan bestes Weigth in {@link EnhancedAIPlayer}
	 *@param move Zu untersuchender Move des Agenten
	 *@param index Index des Threads
	 */
	public WeightThread (Board board, int weight, int index, Move move){
		copyBoard = board.clone();
		copyBoard.make(move);
		this.currentweight = weight;
		this.index= index;
	}

	/**
	 *Erstellt neuen WeightThread.
	 */ 
	public WeightThread (){
		super();
	}

	/**
	 *Berechnet jeden möglichen sinnvollen Gegnerzug und berechnet kleinstes Weight durch {@link WeightThread#testMoves(Board)}.
	 */
	public void run(){
			weight[index] = testMoves(copyBoard);	
	}

	/**Berechnet Gewichtung der Position des aktuellen Spielers, indem er Sites und Links seiner Zusammenhangkomponente
	 *postiv und die des Gegners negativ gewichtet.
	 *@param copyBoard zu verwendenes Board
	 *@return Gewichtung dieser Position
	 */
	private int getWeight(Board copyBoard){
		int score = copyBoard.accessibleSites(copyBoard.getAgent()).size();
		score += copyBoard.accessibleLinks(copyBoard.getAgent()).size();
		score -= copyBoard.accessibleSites(copyBoard.getOpponent()).size();
		score -= copyBoard.accessibleLinks(copyBoard.getOpponent()).size();
		ArrayList<Site> neighbor = copyBoard.getNeighborSites(copyBoard.getAgent());
		score += neighbor.size() / 3;
		for(int i = 0 ; i < neighbor.size() ; i++){
			if(neighbor.get(i) != null){
				score += copyBoard.getNeighborSites(neighbor.get(i)).size() / 3;
			}
		}
		
		return score;
	}

	/**
	 *Berechnet für jeden möglichen Zug des aktuellen Spielers die Gewichtung des Gegners mittels {@link WeightThread#getWeight(Board)}und 
	 *gibt das kleinste davon zurück.
	 *@param copyBoard zu verwendenes Board
	 *@return kleinstes berechnetes Weight
	 */ 
	private int testMoves(Board copyBoard){
		ArrayList<Site> avSites = copyBoard.accessibleSites(copyBoard.getAgent());
		int avSiteNumber = avSites.size();
		int linkNumber;
		ArrayList<SiteSet> links;
		links = copyBoard.getLinks();
		linkNumber = links.size();
		int weight = Integer.MAX_VALUE;
		Site pos = copyBoard.getAgent();

		for(int i = 0 ; i < avSiteNumber ; i++){
			for(int j = 0 ; j < linkNumber ; j++){
				copyBoard.make(new Move(new SiteTuple(pos,avSites.get(i)), links.get(j)));
				int newWeight = getWeight(copyBoard);

				if(newWeight <= currentweight)
					return Integer.MIN_VALUE;
				if(newWeight < weight)
					weight = newWeight;

				copyBoard.reverse(new Move(new SiteTuple(pos,avSites.get(i)), links.get(j)));
			}
		}
		return weight;
	}





}