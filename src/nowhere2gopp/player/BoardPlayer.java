package nowhere2gopp.player;
import nowhere2gopp.preset.*;
import nowhere2gopp.board.*;
import nowhere2gopp.io.*;
/**Klasse die einen Spieler für nowhere2go darstellt.
*Es werden alle Methoden die für jeden lokalen Spieler gleich ablaufen implementiert.
*Ein Spieler erb von dieser Klasse um dann die Funktion {@link Player#request()} zu 
*implementieren.
*Diese Klasse ist nicht für das Spielen über ein Netzwerk geeignet, dazu muss die Klasse
*{@link RemotePlayer} verwendet werden.
*/
public abstract class BoardPlayer implements Player{
	/**Board auf dem jeder {@link BoardPlayer} alle Spielzüge mitschreibt.*/
	protected Board localBoard;
	/**Speichert die Art des Spielers. Kann Werte aus {@link PlayerType} als Enum annehmen.*/
	protected PlayerType type;
	/**Ausgabe des Spielstandes. Wird Standardmässig nur vom Klienten verwendet. Kann aber 
	*über die passende flag auch für jeden Spieler zusätzlich aktiviert werden.*/
	protected Printable output = null;
	/**Eingabe über die der {@link HumanPlayer} seine Züge erhält*/
	protected Requestable input = null;
	/**Flag zum Festlegen ob als Ausgabe des Spielstandes das eigene Board benutzt wird.
	*Ist für den Klienten entscheidend, da nur so der aktuelle Spielstaende gezeigt werden kann.*/
	protected boolean outputUsingMyBoard = false;
	/**Flag zum Festlegen ob die Eingabe über das eigene Board erfolgt. Nur relevant für den 
	* {@link HumanPlayer} als Klient mit grafischer Eingabe.*/
	protected boolean inputUsingMyBoard = false;
	/**Farbe des Spielers und seines Gegners*/
	protected PlayerColor color, opponentColor;
	/**Flag zum Überwachen ob ein Spieler mindestens einmal Initialisiert wurde*/
	protected boolean isInitialized;
	/**Enum zum Überwachen der Methoden Reihenfolge*/
	protected PlayerStatus playerStatus;
	
	/**Konstruiert einen neuen Spieler*/
	public BoardPlayer(){
		super();
	}

	/**Erstellt einen neuen Spieler mit grafischer Ausgabe
	*@param output
	*		übergebene Ausgabe
	*/
	public BoardPlayer(Printable output){
		this.output = output;
	}

	/**Initialisiert einen Spieler mit Farbe und Größe des lokalen Boards.
	*@param boardSize
	*		Größe des lokalen Boards
	*@param color
	*		Farbe des Spielers
	*/
	public void init(int boardSize, PlayerColor color){
		this.color = color;
		if(color == PlayerColor.Red)
			opponentColor = PlayerColor.Blue;
		else
			opponentColor = PlayerColor.Red;

		localBoard = new Board(boardSize);
		/*Setze den Status der Methoden Reihenfolge*/
		playerStatus = PlayerStatus.Updated;
		if (this.color == PlayerColor.Blue) {
			playerStatus = PlayerStatus.Confirmed;
		}
		isInitialized = true;
		if (outputUsingMyBoard) {
			GUI gui = new GUI((BoardViewer) localBoard.viewer());
			output = gui;
				/*Nur relevant wenn ein Humanplayer ein Klient ist.
				In diesem Fall ist der erste flag immer wahr und es sollen 
				nicht zwei Fenster geöffnet werden.*/
				if (inputUsingMyBoard){
				input = gui;
			}
		}
	}

	/**Benötigt den Status der auf dem lokalen Board bestimmt wurde mit Hilfe des übergebenen
	* Status des Hauptspiels.
	*@param status
	*		übergebener Status aus {@link nowhere2gopp.preset.Status}
	*@throws Exception
	*		wenn Fehler bei der Reihenfolge der Mehtoden oder des Spiels auftritt
	*/
	public void confirm(Status status)throws Exception{
		if(!isInitialized)
			throw new Exception("Player muss mit init() initialisiert werden");

		if(playerStatus != PlayerStatus.Requested)
			throw new Exception("Die Reihenfolge beim Methodenaufruf wurde nicht eingehalten" +
				"in confirm");

		if (localBoard.getStatus() != status)
			throw new Exception("Player Status und Board Status stimmen nicht über ein");
		else {
			switch (status) {
				/*In allen drei Fällen muss neu gezeichnet werden, um die Ausgabe immer
				aktuell zu halten*/
				case Ok: 
	            case RedWin: 
	            case BlueWin:
	            	if(output != null){
						output.printBoard();
					}
	                break;

	            case Draw: 
	                System.out.println("Draw");
	                break;

	            case Illegal: 
	                throw new Exception("Illegaler Move");

	            default:
	                throw new Exception("Nicht korrekter Status übergeben und berechnet");
			}
		}
		playerStatus = PlayerStatus.Confirmed;
	}

	/**Aktualisiert das lokale Board mit dem Move des Gegners und überprüft dann den Status das 
	Spiels.
	*@param opponentMove
	*		Spielzug des Gegners
	*@param status
	*		Status des Hauptspiels
	*@throws Exception
	*		wenn Fehler bei der Reihenfolge der Methoden oder des Spiels auftritt
	*/
	public void update(Move opponentMove, Status status) throws Exception{
		if(!isInitialized)
			throw new Exception("Player muss mit init() initialisiert werden");

		if(playerStatus != PlayerStatus.Confirmed)
			throw new Exception("Die Reihenfolge beim Methodenaufruf wurde nicht eingehalten" +
				" in update");
		
		localBoard.make( opponentMove );

		if (localBoard.getStatus() != status)
			throw new Exception("Player Status und Board Status stimmen nicht überein");
		else {
			switch (status) {
				/*In allen drei Fällen muss neu gezeichnet werden, um die Ausgabe immer
				aktuell zu halten*/
				case Ok: 
	            case RedWin: 
	            case BlueWin:
	            	if(output != null){
						output.printBoard();
					}
	                break;

	            case Draw: 
	                System.out.println("Draw");
	                break;

	            case Illegal: 
	                throw new Exception("Illegaler Move");

	            default:
	                throw new Exception("Nicht korrekter Status übergeben und berechnet");
			}
		}
		//Flags für die Kontrolle der Methodenaufrufe neu setzen
		playerStatus = PlayerStatus.Updated;
	}

	/**Gibt den {@link nowhere2gopp.preset.Viewer} des lokalen Boards wieder.
	*@return Viewer
	*		Viewer des lokalen Boards
	*/
	public Viewer viewer(){
		return localBoard.viewer();
	}
}







