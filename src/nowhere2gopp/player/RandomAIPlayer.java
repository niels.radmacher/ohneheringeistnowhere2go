package nowhere2gopp.player;
import nowhere2gopp.preset.*;
import nowhere2gopp.io.Printable;
import java.util.ArrayList;


/**
 *AI Spieler der zufällige Züge zurück gibt 
 */
public class RandomAIPlayer extends BoardPlayer{

	/**
	 *Erstellt neuen RandomAIPlayer, ohne grafischer Ausgabe.
	 */
	public RandomAIPlayer(){
		super();
		this.type = PlayerType.RandomAI;
	}

	/**
	 *Erstellt neuen RandomAIPlayer
	 *@param output Printable Element zur grafischen Ausgabe.
	 */  
	public RandomAIPlayer(Printable output){
		super(output);
		outputUsingMyBoard = true;
		this.type = PlayerType.RandomAI;
	}

	/**
	 *Gibt neuen Move zurück und setzt diesen auf eigenem Board
	 *@return neu berechneter Zug
	 */ 
	public synchronized Move request() throws Exception{
		//nächster Move
		Move nextMove = null;
		Site startsite = null;
		
		
		//erste Spielphase
		switch(localBoard.getGamePhase()){

		case LinkLink :

			int linknumber = localBoard.getLinks().size();
			boolean different = false;
			int ran1 = 0, ran2 = 0;

			while(!different){
				ran1 = (int) (Math.random() * (linknumber-1));
				ran2 = (int) (Math.random() * (linknumber-1));
				if(ran1 != ran2)
					different = true;
			}
			SiteSet link1 = localBoard.getLinks().get(ran1);
			SiteSet link2 = localBoard.getLinks().get(ran2);

			nextMove = new Move(link1, link2);
			break;

		//zweite Spielphase
		case SetAgentLink :

			boolean legal = false;
			int size = localBoard.getSize();

			//Startposition festlegen und schauen ob Zusammenhangskomponente groß genug
			while(!legal){
				ran1 = (int) (Math.random() * (size-1));
				ran2 = (int) (Math.random() * (size-1));
				if(localBoard.isOnBoard(ran1,ran2) && localBoard.accessibleSites(new Site(ran1,ran2)).size() > 1){
					legal = true;
					startsite = new Site(ran1,ran2);
				}
			}
		
		//dritte Spielphase
		case AgentLink:
			

			//Anfangspostion der zweiten Phase nutzen
			Site agent;
			if(startsite != null)
				agent = startsite;
			else
				agent = localBoard.getAgent();

			//Site zum setzen suchen
			ArrayList<Site> sites = localBoard.accessibleSites( agent );
			int sitenumber = sites.size();
			ran1 = (int) (Math.random() * (sitenumber-1));
			Site newsite = sites.get(ran1);
			SiteTuple agentsite = new SiteTuple( agent , newsite);
			
			//Link zum zerstören suchen
			int linknumber2 = localBoard.getLinks().size();
			ran2 = (int) (Math.random() * (linknumber2-1));
			SiteSet link = localBoard.getLinks().get(ran2);
			
			nextMove = new Move(agentsite, link);
			break;
		}

		//Move auf eigenem Board setzen und Move zurückgeben
		localBoard.make(nextMove);
		playerStatus = PlayerStatus.Requested;
		return nextMove;
	}

}