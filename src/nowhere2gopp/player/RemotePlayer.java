package nowhere2gopp.player;
import nowhere2gopp.preset.*;
import java.io.Serializable;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;

/**
 *Wrapper Klasse für Player Objekt. Diese kann einen beliebigen PlayerType sein 
 *der über einen Netzwerk erhalten wird.
 */
public class RemotePlayer extends UnicastRemoteObject implements Player{
	/**Spieler an den alle Funktionen aus {@link nowhere2gopp.preset.Player} weitergeleitet werden.*/
	private Player player;

	/**Erzeugt einen Spieler, der alle Methodenaufrufe an die übergebene Spielerreferenz
	*weiterleitet. Dabei muss diese alle Methoden aus {@link nowhere2gopp.preset.Player}
	*implementieren.
	*@param player
	*		übergeben Spieler
	*@throws RemoteException
	*		wenn es einen Fehler mit der Kommunikation des Netzwerks gab
	*/
	public RemotePlayer(Player player) throws RemoteException{
		this.player = player;
	}

	/**
	*Ruft die Initialisierung beim Übergeben {@link RemotePlayer#player} auf.
	*@param boardSize
	*		Spielbrettgröße mit der der Spieler arbeiten soll
	*@param color
	*		Farbe des Spielers
	*@throws Exception
	*		wenn die Initialisierumg beim übergebenen Spieler einen Fehler hervorruft
	*@throws RemoteException
	*		wenn bei der Kommumikation mit dem Netztwerk ein Fehler auftritt
	*/
	public void init(int boardSize, PlayerColor color) throws Exception, RemoteException{
		player.init(boardSize, color);
	}

	/**
	*Fragt nach einem neuen Zug beim Übergeben {@link RemotePlayer#player}.
	*
	*@throws Exception
	*		wenn bei der Nachfrage ein Fehler hervorgeruen wird
	*@throws RemoteException
	*		wenn bei der Kommumikation mit dem Netzwerk ein Fehler auftritt
	*/
	public Move request() throws Exception, RemoteException{
		return player.request();
	}

	/**
	*Bestätigt den letzten Zug des übergebenen {@link RemotePlayer#player} mit dem Status 
	*des Hauptspiels.
	*@param status
	*		Status mit dem verglichen wird
	*@throws Exception
	*		wenn bei der Bestätigung ein Fehler hervorgerufen wird
	*@throws RemoteException
	*		wenn bei der Kommumikation mit dem Netzwerk ein Fehler auftritt
	*/
	public void confirm(Status status) throws Exception, RemoteException{
		player.confirm(status);
	}

	/**
	*Aktualisiert das lokale Board mit den letzten Zug des Hauptspiels und vergleicht mit 
	*diesem. 
	*@param opponentMove
	*		Zug der vom Gegner gemacht wurde
	*@param status
	*		Status mit dem verglichen wird
	*@throws Exception
	*		wenn bei der Aktualisierung ein Fehler hervorgerufen wird
	*@throws RemoteException
	*		wenn bei der Kommumikation mit dem Netzwerk ein Fehler auftritt
	*/
	public void update(Move opponentMove, Status status) throws Exception, RemoteException{
		player.update(opponentMove, status);
	}
}







