package nowhere2gopp.preset;

import java.util.*;

/**
 * Ein simpler Parser fuer Kommandozeilen Parameter.
 * <h1>Verwendung</h1>
 * <p>
 * Erzeuge innerhalb deiner ausfuehrbaren Klasse eine Instanz dieser Klasse und 
 * uebergib im Konstruktor die Kommandozeilenargumente. 
 * Verwende diesen ArgumentParser um auf Kommandozeilen Parameter zu reagieren.
 * </p>
 * <p>
 * Kommandozeilen Parameter sind entweder Schalter oder Einstellungen.
 * </p>
 * <h2>Schalter</h2>
 * <p>
 * Ein Schalter ist entweder ein- oder ausgeschaltet. Dementsprechend kann sein 
 * Zustand in einem {@code boolean} abgelegt werden. Schalter sind zu Beginn 
 * ausgeschaltet. Ein Schalter wird ueber den Parameter {@code --SCHALTERNAME}
 * aktiviert. Ein Schalter kann ueber Kommandozeilen Parameter nicht deaktiviert 
 * werden, da er zu Beginn ohnehin deaktiviert ist.
 * </p>
 * <h2>Einstellungen</h2>
 * <p>
 * Eine Einstellung hat einen Namen und einen Wert. Ein gutes Beispiel ist hier 
 * die Spielfeldgroesse. Der Name dieser Einstellung ist {@code size} und der Wert 
 * kann eine Zahl zwischen {@code 6} und {@code 26} sein. Der Typ einer
 * Einstellung richtet sich nach der Einstellung. Die Einstellung {@code size} 
 * zum Beispiel ist ein {@code int}. Einstellungen werden auf der Kommandozeile 
 * mit {@code -NAME WERT} gesetzt.
 * </p>
 * <p>
 * Wird ein Schalter oder eine Einstellung abgefragt die nicht eingelesen wurde, 
 * wird eine {@link ArgumentParserException} geworfen, auf die sinnvoll reagiert 
 * werden muss.
 * </p>
 * <p>
 * Alle Schalter und Einstellungen in dieser Klasse duerfen nicht geaendert werden. 
 * Es ist jedoch erlaubt weitere Schalter oder Einstellungen hinzuzufuegen, dies ist 
 * im Quellcode kenntlich gemacht.
 * </p>
 *
 * @author Dominick Leppich
 */
public class ArgumentParser {
    /** Map zur Speicherung der Parameter */
    private HashMap<String, Object> params;

    // ------------------------------------------------------------

    /**
     * Erzeuge einen neuen ArgumentParser aus einem String-Array mit Parametern. 
     * Hier sollte einfach das {@code args} Argument der {@code main}-Methode 
     * weitergerreicht werden.
     *
     * @param args
     *         Argumente
     *
     * @throws ArgumentParserException
     *         wenn das Parsen der Argumente fehlschlaegt
     */
    public ArgumentParser(final String[] args) throws ArgumentParserException {
        params = new HashMap<>();
        parseArgs(args);
    }
    // ------------------------------------------------------------

    /**
     * Parse die Argumente.
     *
     * @param args
     *         Argumente
     *
     * @throws ArgumentParserException
     *         wenn das Parsen der Argumente fehlschlaegt
     */
    private void parseArgs(final String[] args) throws ArgumentParserException {
        // Index to parse
        int index = 0;

        try {
            while (index < args.length) {
                // Check if argument is a flag or setting
                if (args[index].startsWith("--")) {
                    addFlag(args[index].substring(2));
                    index += 1;
                } else if (args[index].startsWith("-")) {
                    addSetting(args[index].substring(1), args[index + 1]);
                    index += 2;
                } else
                    throw new ArgumentParserException("Error parsing: " + args[index]);
            }
        } catch (IndexOutOfBoundsException e) {
            throw new ArgumentParserException("Missing parameter", e);
        }
    }

    /**
     * Fuege einen Schalter hinzu.
     *
     * @param flag
     *         Schalte
     *
     * @throws ArgumentParserException
     *         wenn der Schalter nicht existiert
     */
    private void addFlag(final String flag) throws ArgumentParserException {
        // Check if a param with this name already exists
        if (params.containsKey(flag))
            throw new ArgumentParserException("Param already exists: " + flag);

        params.put(flag, Boolean.TRUE);
    }

    /**
     * Fuege eine Einstellung hinzu.
     *
     * @param key
     *         Name
     * @param value
     *         Wert
     *
     * @throws ArgumentParserException
     *         wenn die Einstellung nicht existiert oder der Wert ein ungueltiges 
     *         Format hat
     */
    private void addSetting(final String key, final String value) throws ArgumentParserException {
        // Check if a param with this name already exists
        if (params.containsKey(key))
            throw new ArgumentParserException("Param already exists: " + key);

        if (value.startsWith("-"))
            throw new ArgumentParserException("Setting value wrong format: " + value);

        params.put(key, value);
    }

    // ------------------------------------------------------------

    /**
     * Pruefe ob ein Parameter gesetzt ist.
     *
     * @param parameter
     *         Zu pruefender Parameter
     *
     * @return wahr, wenn der Parameter gesetzt wurde
     */
    public boolean isSet(final String parameter) {
        return params.containsKey(parameter);
    }

    /**
     * Gib den Wert eines Schalters zurueck.
     *
     * @param flag
     *         Name des Schalters
     *
     * @return Wert
     *
     * @throws ArgumentParserException
     *         wenn der Schalter den falschen Typ hat (falls eine Einstellung 
     *         versucht wird als Schalter auszulesen)
     */
    protected boolean getFlag(final String flag) throws ArgumentParserException {
        if (!params.containsKey(flag))
            return false;

        Object o = params.get(flag);
        if (!(o instanceof Boolean))
            throw new ArgumentParserException("This is not a flag");

        return (Boolean) params.get(flag);
    }

    /**
     * Gib den Wert einer Einstellung als {@link Object} zurueck.
     *
     * @param key
     *         Name der Einstellung
     *
     * @return Wert als {@link Object}.
     *
     * @throws ArgumentParserException
     *         wenn die Einstellung nicht existiert
     */
    protected Object getSetting(final String key) throws ArgumentParserException {
        if (!params.containsKey(key))
            throw new ArgumentParserException("Setting " + key + " not " + "defined");

        return params.get(key);
    }

    // ------------------------------------------------------------

    /**
     * Interpretiere einen Spielertypen
     *
     * @param type
     *         Eingelesener Typ
     *
     * @return Spielertyp als {@link PlayerType}
     *
     * @throws ArgumentParserException
     *         wenn der eingelese Typ nicht passt
     */
    protected PlayerType parsePlayerType(final String type) throws ArgumentParserException {
        switch (type) {
            case "human":
                return PlayerType.Human;
            case "random":
                return PlayerType.RandomAI;
            case "simple":
                return PlayerType.SimpleAI;
            case "extended":
                return PlayerType.ExtendedAI;
            case "upgraded":
                return PlayerType.UpgradedAI;
            case "enhanced":
                return PlayerType.EnhancedAI;
            case "advanced":
                return PlayerType.AdvancedAI;
            case "remote":
                return PlayerType.Remote;
            default:
                throw new ArgumentParserException("Unknown player type: " + type);
        }
    }

    // ------------------------------------------------------------

    /**Liefert die Größe die mit dem Einstellung size übergeben wurde.
    *@return Größe des Spielfeldes
    *@throws ArgumentParserException
    *         wenn die Einstellung nicht gesetzt ist
    */
    public int getSize() throws ArgumentParserException {
        return Integer.parseInt((String) getSetting("size"));
    }

    /**Liefert den {@link nowhere2gopp.preset.PlayerType} die mit dem Einstellung red übergeben wurde.
    *@return Spieler Type
    *@throws ArgumentParserException
    *         wenn die Einstellung nicht gesetzt ist
    */
    public PlayerType getRed() throws ArgumentParserException {
        return parsePlayerType((String) getSetting("red"));
    }

    /**Liefert den {@link nowhere2gopp.preset.PlayerType} die mit dem Einstellung blue übergeben wurde.
    *@return Spieler Type
    *@throws ArgumentParserException
    *         wenn die Einstellung nicht gesetzt ist
    */
    public PlayerType getBlue() throws ArgumentParserException {
        return parsePlayerType((String) getSetting("blue"));
    }

    /**Liefert den {@link nowhere2gopp.preset.PlayerType} die mit dem Einstellung offer übergeben wurde.
    *@return Spieler Type
    *@throws ArgumentParserException
    *         wenn die Einstellung nicht gesetzt ist
    */
    public PlayerType getOffer() throws ArgumentParserException {
        return parsePlayerType((String) getSetting("offer"));
    }

    /**Liefert die Wartezeit die mit dem Einstellung delay übergeben wurde.
    *@return Wartezeit
    *@throws ArgumentParserException
    *         wenn die Einstellung nicht gesetzt ist
    */
    public int getDelay() throws ArgumentParserException {
        return Integer.parseInt((String) getSetting("delay"));
    }

    public boolean isDebug() throws ArgumentParserException {
        return getFlag("debug");
    }
    // ********************************************************************
    //  Hier koennen weitere Schalter und Einstellungen ergaenzt werden...
    // ********************************************************************

    /**Liefert den Namen des ersten Hoste.
    *@return Host
    *@throws ArgumentParserException
    *         wenn die Einstellung nicht gesetzt ist
    */
    public String getHost() throws ArgumentParserException {
        return (String) getSetting("host");
    }

    /**Liefert den Namen des zweiten Hoste.
    *@return Host
    *@throws ArgumentParserException
    *         wenn die Einstellung nicht gesetzt ist
    */
    public String getHost2() throws ArgumentParserException {
        return (String) getSetting("host2");
    }

    /**Liefert den Port die mit dem Einstellung port übergeben wurde.
    *@return Port
    *@throws ArgumentParserException
    *         wenn die Einstellung nicht gesetzt ist
    */
    public int getPort() throws ArgumentParserException {
        return Integer.parseInt((String) getSetting("port"));
    }

    /**Liefert den Port die mit dem Einstellung port2 übergeben wurde.
    *@return zweiter Port
    *@throws ArgumentParserException
    *         wenn die Einstellung nicht gesetzt ist
    */
    public int getPort2() throws ArgumentParserException {
        return Integer.parseInt((String) getSetting("port2"));
    }

    /**Ist wahr wenn der Schalter client2 gesetz wurde. Sollte verwendet werden um zu markieren,
    *dass es sich um den Zweiten Klienten handel des über ein Netzwerk angeboten wird.
    *@return whar wenn der Schalter gesetz ist
    *@throws ArgumentParserException
    *         wenn der Schalter nicht gesetzt ist
    */
    public boolean secondClient() throws ArgumentParserException {
        return getFlag("client2");
    }

    /**Ist wahr wenn der Schalter nografic gesetz wurde. Sollte verwendet werden um zu markieren,
    *dass keine zusäzliche grafische Ausgabe verwendet wird.
    *@return whar wenn der Schalter gesetz ist
    *@throws ArgumentParserException
    *         wenn der Schalter nicht gesetzt ist
    */
    public boolean noGrafic() throws ArgumentParserException{
        return getFlag("nografic");
    }

    /**Ist wahr wenn der Schalter grafic gesetz wurde. Sollte verwendet werden um zu markieren,
    *dass eine zusäzliche grafische Ausgabe verwendet wird.
    *@return whar wenn der Schalter gesetz ist
    *@throws ArgumentParserException
    *         wenn der Schalter nicht gesetzt ist
    */
    public boolean grafic() throws ArgumentParserException{
        return getFlag("grafic");
    }

    /**Ist wahr wenn der Schalter textinput gesetz wurde. Sollte verwendet werden um zu markieren,
    *dass eine menschlicher Spieler über die Texteingabe seinen Züge erhält.
    *@return whar wenn der Schalter gesetz ist
    *@throws ArgumentParserException
    *         wenn der Schalter nicht gesetzt ist
    */
    public boolean textinput() throws ArgumentParserException{
        return getFlag("textinput");
    }
}
