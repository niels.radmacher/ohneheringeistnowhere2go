package nowhere2gopp.preset;

import java.util.*;

import nowhere2gopp.board.GamePhase;

public interface Viewer {

    /** liefert den Spieler welcher am Zug ist
     * @return die Farbe des Spilers am Zug*/
    PlayerColor getTurn();

    /** die Größe des Brettes
     * @return die Größe*/
    int getSize();

    /** liefert den Status des spieles
     * @return den aktuellen Status */
    Status getStatus();

    /** liefert die Postion des Spielers
     * @param color die Farbe des Spielers dessen positon gesucht wird
     * @return die Site auf der er sich befindet */
    Site getAgent(final PlayerColor color);

    /** exitierende links 
     * @return alle existierenden links*/
    Collection<SiteSet> getLinks();

    /** gibt die Runden Anzahl zurück
     * @return die Anzahl der Runden */
    int getRoundNumber();

    /** gibt die momentan von der ausgewählte Site betretbaren Sites zurück
     * @param start die Site von der aus gesucht werden soll
     * @return eine Liste aller betretbaren Sites*/
    ArrayList<Site> getAccessableSites(Site start);

    /** gibt die spielphase des spieles zurück 
     * @return die aktuelle Spielphase*/
    GamePhase getGamePhase();
}
